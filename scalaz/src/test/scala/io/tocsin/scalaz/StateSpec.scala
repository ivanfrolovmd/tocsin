package io.tocsin.scalaz

import io.tocsin.core.Collector
import org.scalatest.{FreeSpec, Matchers, OneInstancePerTest}
import scalaz.Kleisli
import scalaz.Scalaz._

class StateSpec extends FreeSpec with OneInstancePerTest with Matchers {
  implicit val timeline: Timeline = Timeline()

  "functor" - {
    "map should morph the value" in {
      val source = Source[Int]
      val mapped = source.signal.hold(100).map(_ + 5)
      val probe  = mapped.probe

      probe.sample shouldBe 105
      source ! 10
      probe.sample shouldBe 15
      source ! 20
      probe.sample shouldBe 25
    }

    "Law 1: Identity" in {
      val source = Source[Int]
      val mapped = source.signal.hold.map(identity)
      val probe  = mapped.probe

      probe.sample shouldBe 0
      source ! 10
      probe.sample shouldBe 10
      source ! 20
      probe.sample shouldBe 20
    }

    "Law 2: Composition of morphisms" in {
      val source = Source[Int]
      val state  = source.signal.hold

      val f          = (i: Int) => i + 5
      val g          = (i: Int) => i * 2
      val fThenG     = state.map(f).map(g)
      val fgComposed = state.map(f andThen g)
      val probe1     = fThenG.probe
      val probe2     = fgComposed.probe

      probe1.sample shouldBe probe2.sample
      probe1.sample shouldBe (0 + 5) * 2

      source ! 1
      probe1.sample shouldBe probe2.sample
      probe1.sample shouldBe (1 + 5) * 2

      source ! 2
      probe1.sample shouldBe probe2.sample
      probe1.sample shouldBe (2 + 5) * 2
    }
  }

  "applicative" - {
    "merged states should include updates from both upstream states" in {
      val source1 = Source[String]
      val source2 = Source[Int]

      val state1 = source1.signal.hold
      val state2 = source2.signal.hold

      val tupled  = state1 tuple state2
      val probe   = tupled.probe
      val updates = Collector(tupled)

      probe.sample shouldBe ("", 0)

      source1 ! "one"
      probe.sample shouldBe ("one", 0)

      source2 ! 1
      probe.sample shouldBe ("one", 1)

      timeline.tx {
        source2 ! 2
        source1 ! "two"
      }
      probe.sample shouldBe ("two", 2)

      updates.events shouldBe List(("one", 0), ("one", 1), ("two", 2))
    }

    "should have semigroup/monoid instance if underlying type has semigroup/monoid instance" in {
      val source1 = Source[Int]
      val source2 = Source[Int]

      val state1 = source1.signal.hold
      val state2 = source2.signal.hold

      val semigroup      = state1 |+| state2 |+| 100.pure[State]
      val semigroupProbe = semigroup.probe
      val monoid         = List(state1, state2, 100.pure[State]).suml
      val monoidProbe    = monoid.probe

      semigroupProbe.sample shouldBe 100
      monoidProbe.sample shouldBe 100

      source1 ! 10
      semigroupProbe.sample shouldBe 110
      monoidProbe.sample shouldBe 110

      source2 ! -20
      semigroupProbe.sample shouldBe 90
      monoidProbe.sample shouldBe 90

      timeline.tx {
        source1 ! 1
        source2 ! 2
      }
      semigroupProbe.sample shouldBe 103
      monoidProbe.sample shouldBe 103
    }

    "Law 1: Identity" in { //pure id <*> v = v
      val vSource       = Source[Int]
      val v: State[Int] = vSource.signal.hold

      val leftSide = v <*> (identity[Int] _).pure[State]
      val left     = leftSide.probe
      val right    = v.probe

      left.sample shouldBe right.sample
      left.sample shouldBe 0

      vSource ! 10
      left.sample shouldBe right.sample
      left.sample shouldBe 10
    }

    "Law 1: Identity expressed as monoid" in {
      val source = Source[Int]
      val state  = source.signal.hold

      val leftSide  = 0.pure[State] |+| state
      val left      = leftSide.probe
      val rightSide = state |+| 0.pure[State]
      val right     = rightSide.probe

      left.sample shouldBe right.sample
      left.sample shouldBe 0

      source ! 10
      left.sample shouldBe right.sample
      left.sample shouldBe 10
    }

    "Law 2: Homomorphism" in { //pure f <*> pure x = pure (f x)
      val f = (i: Int) => i + 5
      val x = 10

      val (left, right) = timeline.tx {
        val leftSide  = x.pure[State] <*> f.pure[State]
        val rightSide = f(x).pure[State]
        (leftSide.probe(timeline), rightSide.probe(timeline))
      }

      left.sample shouldBe right.sample
      left.sample shouldBe 15
    }

    "Law 3: Interchange" in { //u <*> pure y = pure ($ y) <*> u
      val u = ((i: Int) => i + 5).pure[State]
      val y = 10

      val (left, right) = timeline.tx {
        val leftSide  = 10.pure[State] <*> u
        val rightSide = u <*> ((f: Int => Int) => f(y)).pure[State]
        (leftSide.probe(timeline), rightSide.probe(timeline))
      }

      left.sample shouldBe right.sample
      left.sample shouldBe 15
    }

    "Law 4: Composition" in { //pure (.) <*> u <*> v <*> w = u <*> (v <*> w)
      val uSource = Source[Int => Int]
      val vSource = Source[Int => Int]
      val wSource = Source[Int]

      val u   = uSource.signal.hold(identity[Int] _)
      val v   = vSource.signal.hold(identity[Int] _)
      val w   = wSource.signal.hold(100)
      val dot = ((f: Int => Int) => (g: Int => Int) => f compose g).pure[State]

      val leftSide  = w <*> (v <*> (u <*> dot))
      val rightSide = (w <*> v) <*> u
      val left      = leftSide.probe
      val right     = rightSide.probe

      left.sample shouldBe right.sample
      left.sample shouldBe 100

      timeline.tx {
        uSource ! ((i: Int) => i * 2)
        vSource ! ((i: Int) => i + 5)
        wSource ! 10
      }

      left.sample shouldBe right.sample
      left.sample shouldBe (10 + 5) * 2
    }
  }
  "monad" - {
    "Law 1: Left identity" in { // return a >>= f = f a
      val source = Source[Int]

      val state = source.signal.hold

      val f = (i: Int) => state |+| i.pure[State]
      val a = 10

      val left  = (a.pure[State] >>= f).probe(timeline)
      val right = f(a).probe(timeline)

      left.sample shouldBe right.sample
      left.sample shouldBe 10

      source ! 100
      left.sample shouldBe right.sample
      left.sample shouldBe 110
    }

    "Law 2: Right identity" in { // m >>= return = m
      val source = Source[Int]

      val state = source.signal.hold >>= (a => a.pure[State])
      val probe = state.probe

      probe.sample shouldBe 0

      source ! 1
      probe.sample shouldBe 1
    }

    "Law 3: Associativity" in {
      // (m >>= f) >>= g = m >>= (\x -> f x >>= g)
      // or:
      // (f >=> g) >=> h = f >=> (g >=> h)

      import scala.language.{higherKinds, implicitConversions}
      implicit def toKleisli[M[_], A, B](f: A => M[B]): Kleisli[M, A, B] = Kleisli(f)

      val source = Source[Int]
      val state  = source.signal.hold(1000)

      val f = (i: Int) => State(i) |+| state
      val g = (i: Int) => (i + 5).pure[State]
      val h = (i: Int) => State(i)

      val leftComposed  = (f >=> g) >=> h
      val rightComposed = f >=> (g >=> h)

      val (left, right) = timeline.tx {
        val leftSide  = leftComposed(10)
        val rightSide = rightComposed(10)
        source ! 1
        (leftSide.probe(timeline), rightSide.probe(timeline))
      }

      left.sample shouldBe right.sample
      left.sample shouldBe 16

      source ! 2

      left.sample shouldBe right.sample
      left.sample shouldBe 17
    }

    "switching state use case" in {
      val switch = Source[Int]

      val source1 = Source[Int]
      val source2 = Source[Int]

      val state1 = source1.signal.hold(1)
      val state2 = source2.signal.hold(100)
      val state3 = state2.map(_ + 1000)

      val sinkState = switch.signal.hold >>= {
        case 1 => state1
        case 2 => state2
        case 3 => state3
        case _ => State(-2)
      }

      val probe   = sinkState.probe
      val updates = Collector(sinkState)

      // initial holding
      probe.sample shouldBe -2

      // non-connected states don't change sink's result
      source1 ! 2
      probe.sample shouldBe -2

      // when switching to other states the result changes as well
      switch ! 1
      probe.sample shouldBe 2
      switch ! 2
      probe.sample shouldBe 100
      switch ! 3
      probe.sample shouldBe 1100

      // when changing connected state, the result changes as well
      source2 ! 200
      probe.sample shouldBe 1200

      // when returned to constant, display constant's value
      switch ! 10
      probe.sample shouldBe -2

      // when changing connected states, updates should be fired even if they render equal results
      source1 ! 77
      source2 ! 77
      probe.sample shouldBe -2
      switch ! 1
      probe.sample shouldBe 77
      switch ! 2
      probe.sample shouldBe 77

      updates.events shouldBe List(2, 100, 1100, 1200, -2, 77, 77)
    }
  }
}
