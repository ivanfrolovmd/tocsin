package io.tocsin.plain

import io.tocsin.core
import io.tocsin.core.Primitives

class Source[A](val source: core.Source[A]) {
  def signal: Signal[A]          = Primitives.signal[A](source)
  def send[AA <: A](a: AA): Unit = Primitives.send[A, AA](source, a)
  def ![AA <: A](a: AA): Unit    = send(a)
  def ![AA <: A](as: AA*): Unit  = for (a <- as) send(a)
}
object Source {
  def apply[A](implicit timeline: Timeline): Source[A] = Primitives.createSource[A](timeline)
}
