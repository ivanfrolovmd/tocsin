package io.tocsin

import io.tocsin.core.{Conversions, Primitives}

import scala.concurrent.ExecutionContext

package object scalaz extends StateOps with SignalOps with SignalInstances with StateInstances with Conversions {
  implicit val timelineConv: Timeline <=> core.Timeline = iso(_.timeline, new Timeline(_))
  class Timeline(val timeline: core.Timeline) {
    def tx[Z](code: => Z): Z = Primitives.tx(timeline, code)
  }
  object Timeline {
    def apply(): Timeline = Primitives.createTimeline
  }

  object Transaction {
    def apply[A](code: => A)(implicit timeline: Timeline): A = timeline.tx(code)
  }

  implicit def source[A]: Source[A] <=> core.Source[A] = iso(_.source, new Source(_))
  class Source[A](val source: core.Source[A]) {
    def signal: Signal[A]          = Primitives.signal(source)
    def send[AA <: A](a: AA): Unit = Primitives.send(source, a)
    def ![AA <: A](a: AA): Unit    = send(a)
    def ![AA <: A](as: AA*): Unit  = for (a <- as) send(a)
  }
  object Source {
    def apply[A](implicit timeline: Timeline): Source[A] = Primitives.createSource[A](timeline)
  }

  implicit def signal[A]: Signal[A] <=> core.Signal[A] = iso(_.signal, new Signal(_))
  class Signal[A](val signal: core.Signal[A])
  object Signal {
    def never[A]: Signal[A]            = Primitives.signalNever[A]
    def once[A](value: A): Signal[A]   = Primitives.signalOnce(value)
    def always[A](value: A): Signal[A] = Primitives.signalAlways(value)
  }

  implicit def state[A]: State[A] <=> core.State[A] = iso(_.state, new State(_))
  class State[A](val state: core.State[A])
  object State {
    def apply[A](a: A): State[A] = Primitives.stateConst(a)
  }

  implicit def sink[A]: Sink[A] <=> core.Sink[A] = iso(_.sink, new Sink(_))
  class Sink[A](val sink: core.Sink[A]) {
    def listen(cb: A => Unit): Listener                                     = Primitives.listen(sink, cb)
    def foreach(cb: A => Unit): Unit                                        = listen(cb)
    def listenWeak(cb: A => Unit): Listener                                 = Primitives.listenWeak(sink, cb)
    def listenOnce(cb: A => Unit): Listener                                 = Primitives.listenOnce(sink, cb)
    def listenOnceWeak(cb: A => Unit): Listener                             = Primitives.listenOnceWeak(sink, cb)
    def listenAsync(cb: A => Unit)(implicit ec: ExecutionContext): Listener = Primitives.listenAsync(sink, cb, ec)
  }

  implicit val listener: Listener <=> core.Listener = iso(_.listener, new Listener(_))
  class Listener(val listener: core.Listener) {
    def unregister(): Unit = Primitives.unregister(listener)
  }

  implicit def probe[A]: Probe[A] <=> core.Probe[A] = iso(_.probe, new Probe(_))
  class Probe[A](val probe: core.Probe[A]) {
    def sample: A = Primitives.sample(probe)
  }
}
