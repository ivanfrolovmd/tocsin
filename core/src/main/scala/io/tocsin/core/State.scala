package io.tocsin.core

import io.tocsin.core.Graph.StateNode

private[tocsin] sealed trait State[A]

private class StateImpl[A](val node: StateNode[A]) extends State[A]
