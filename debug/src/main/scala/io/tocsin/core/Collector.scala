package io.tocsin.core

import io.tocsin.core.Primitives._

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class Collector[A](sink: Sink[A]) {
  private val as: mutable.Buffer[A] = new ArrayBuffer[A]()
  listen[A](sink, as += _)
  def events: List[A] = as.toList
}

object Collector {
  def apply[A](signal: Signal[A]): Collector[A]                     = new Collector(sink(signal))
  def apply[A](state: State[A]): Collector[A]                       = new Collector(sink(stateUpdates(state)))
  def apply[A](signal: Signal[A], timeline: Timeline): Collector[A] = new Collector(sink(signal, timeline))
  def apply[A](state: State[A], timeline: Timeline): Collector[A]   = new Collector(sink(stateUpdates(state), timeline))
}
