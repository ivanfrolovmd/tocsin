package io.tocsin.plain

import io.tocsin.core
import io.tocsin.core.Primitives

import scala.concurrent.ExecutionContext

class Sink[A](val sink: core.Sink[A]) {
  def listen(cb: A => Unit): Listener                                     = Primitives.listen[A](sink, cb)
  def foreach(cb: A => Unit): Unit                                        = listen(cb)
  def listenWeak(cb: A => Unit): Listener                                 = Primitives.listenWeak[A](sink, cb)
  def listenOnce(cb: A => Unit): Listener                                 = Primitives.listenOnce[A](sink, cb)
  def listenOnceWeak(cb: A => Unit): Listener                             = Primitives.listenOnceWeak[A](sink, cb)
  def listenAsync(cb: A => Unit)(implicit ec: ExecutionContext): Listener = Primitives.listenAsync(sink, cb, ec)
}
