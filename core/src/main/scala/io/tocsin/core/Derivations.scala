package io.tocsin.core

import scala.reflect.ClassTag

object Derivations {
  import Primitives._

  def collectOnly[A, AA <: A](signal: Signal[A], cl: ClassTag[AA]): Signal[AA] =
    signalFilter[A](signal, a => cl.runtimeClass.isInstance(a)).asInstanceOf[Signal[AA]]
  def collect[A, B](signal: Signal[A], f: PartialFunction[A, B]): Signal[B] =
    signalMap(signalFilter(signal, f.isDefinedAt), f)

  def partition[A](signal: Signal[A], p: A => Boolean): (Signal[A], Signal[A]) =
    (signalFilter(signal, p), signalFilter(signal, a => !p(a)))

  def accumulate[A, B](signal: Signal[A], zero: B, work: (A, B) => B): State[B] = {
    lazy val newValue        = signalSnapshot(signal, accum, work)
    lazy val accum: State[B] = stateHold(newValue, zero)
    accum
  }

  def scan[A, B](signal: Signal[A], zero: B, work: (A, B) => B): Signal[B] =
    stateUpdates(accumulate(signal, zero, work))

  def count[A](signal: Signal[A]): State[Int] = accumulate[A, Int](signal, 0, (_, c) => c + 1)

  def zipWithPrevious[A](signal: Signal[A], zero: A): Signal[(A, A)] =
    signalSnapshot[A, A, (A, A)](signal, stateHold(signal, zero), (a, b) => (b, a))

  def zipWithIndex[A](signal: Signal[A]): Signal[(A, Int)] =
    signalSnapshot[A, Int, (A, Int)](signal, count(signal), Tuple2.apply)

  def takeWhile[A](signal: Signal[A], predicate: A => Boolean): Signal[A] = {
    val gate = accumulate[A, Boolean](signal, true, (a, open) => open && predicate(a))
    signalWithGate(signalFilter(signal, predicate), gate)
  }
  def dropWhile[A](signal: Signal[A], predicate: A => Boolean): Signal[A] = {
    val gate = accumulate[A, Boolean](signal, false, (a, open) => open || !predicate(a))
    signalPlus(signalWithGate(signal, gate), signalFilter(signal, a => !predicate(a)))
  }

  def take[A](signal: Signal[A], n: Int): Signal[A] = {
    val gate = stateMap[Int, Boolean](count(signal), _ < n)
    signalWithGate[A](signal, gate)
  }
  def drop[A](signal: Signal[A], n: Int): Signal[A] = {
    val gate = stateMap[Int, Boolean](count(signal), _ >= n)
    signalWithGate[A](signal, gate)
  }
  def slice[A](signal: Signal[A], from: Int, until: Int): Signal[A] = {
    val gate = stateMap[Int, Boolean](count(signal), c => from <= c && c < until)
    signalWithGate[A](signal, gate)
  }

  def sliding[A](signal: Signal[A], size: Int): Signal[Seq[A]] = {
    // TODO this can be optimized by using append-only data structure similar to EphemeralStream or cons with weak references
    lazy val newValue             = signalSnapshot[A, Seq[A], Seq[A]](signal, accum, (a, as) => as.drop(as.size - size + 1) :+ a)
    lazy val accum: State[Seq[A]] = stateHold(newValue, Seq())
    newValue
  }

  def grouped[A](signal: Signal[A], size: Int): Signal[Seq[A]] = {
    lazy val newValue = signalSnapshot[A, (Int, List[A]), (Int, List[A])](signal, accum, {
      case (a, (`size`, _))    => (1, a :: Nil)
      case (a, (length, list)) => (length + 1, a :: list)
    })
    lazy val accum: State[(Int, List[A])] = stateHold(newValue, (0, Nil))
    signalMap[(Int, List[A]), Seq[A]](signalFilter(newValue, _._1 == size), _._2.reverse)
  }
}
