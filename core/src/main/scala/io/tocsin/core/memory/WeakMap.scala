package io.tocsin.core.memory

import scala.collection.convert.Wrappers.{JMapWrapper, JMapWrapperLike}

class WeakMap[A, B] private (defaultValueImpl: A => B)
    extends JMapWrapper[A, B](new java.util.WeakHashMap)
    with JMapWrapperLike[A, B, WeakMap[A, B]] {

  def this() = this((key: A) => throw new NoSuchElementException("key not found: " + key))
  def this(defaultValue: => B) = this((_: A) => defaultValue)

  override def default(key: A): B = defaultValueImpl(key)
  override def empty              = new WeakMap[A, B](defaultValueImpl)
}
