package io.tocsin

import io.tocsin.scalaz.{Signal, State}
import org.scalatest.Assertions

import scala.concurrent.duration._
import scala.language.postfixOps

package object utils {
  implicit class SignalOps2[A](signal: Signal[A]) {
    import io.tocsin.scalaz.signalInstance.functorSyntax._
    def deepen(levels: Int): Signal[A] = {
      var start = signal
      for (_ <- 1 to levels) start = start.map(identity)
      start
    }
  }

  implicit class StateOps2[A](signal: State[A]) {
    import io.tocsin.scalaz.stateInstance.functorSyntax._
    def deepen(levels: Int): State[A] = {
      var start = signal
      for (_ <- 1 to levels) start = start.map(identity)
      start
    }
  }

  def await(time: FiniteDuration): Await = new Await(time)
  class Await(time: FiniteDuration) {
    def until(predicate: => Boolean): Unit = {
      val deadline = Deadline.now + time
      while (!predicate && deadline.hasTimeLeft()) {
        Thread.sleep(100)
      }
      if (deadline.isOverdue()) Assertions.fail("Timed out waiting on predicate")
    }
  }

  def gcAndExpect(condition: => Boolean): Unit = {
    System.gc()
    await(10 seconds).until(condition)
  }
}
