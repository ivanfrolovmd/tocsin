package io.tocsin.scalaz

import io.tocsin.core.{Collector, NewTransactionRequired}
import org.scalatest.{FreeSpec, Matchers, OneInstancePerTest}
import scalaz.Scalaz._

import scala.collection.mutable.ListBuffer

class EgressSpec extends FreeSpec with OneInstancePerTest with Matchers {
  implicit val timeline: Timeline = Timeline()

  "sink" - {
    "listener can be registered and unregistered" in {
      val source = Source[Int]
      val signal = source.signal
      val events = new ListBuffer[Int]

      source ! (1, 2, 3)
      val listener = signal.sink.listen(events += _)
      source ! (4, 5, 6) // only these get to listener's callback
      listener.unregister()
      source ! (7, 8, 9)
      listener.unregister() // nothing happens
      source ! (11, 12, 13)

      events shouldBe List(4, 5, 6)
    }

    "listenWeak can be registered and unregistered" in {
      val source = Source[Int]
      val signal = source.signal
      val events = new ListBuffer[Int]

      source ! (1, 2, 3)
      val listener = signal.sink.listenWeak(events += _)
      source ! (4, 5, 6) // only these get to listener's callback
      listener.unregister()
      source ! (7, 8, 9)
      listener.unregister() // nothing happens
      source ! (11, 12, 13)

      events shouldBe List(4, 5, 6)
    }

    "listenOnce fires only once" in {
      val source = Source[Int]
      val signal = source.signal
      val events = new ListBuffer[Int]

      source ! (1, 2, 3)
      val listener = signal.sink.listenOnce(events += _)
      source ! (4, 5, 6) // only "4" gets to the callback
      listener.unregister() // nothing happens
      source ! (7, 8, 9)

      events shouldBe List(4)
    }

    "listenOnce can be unregistered before it has fired" in {
      val source = Source[Int]
      val signal = source.signal
      val events = new ListBuffer[Int]

      source ! (1, 2, 3)
      val listener = signal.sink.listenOnce(events += _)
      listener.unregister()
      source ! (4, 5, 6)

      events shouldBe List()
    }

    "listenOnceWeak fires only once" in {
      val source = Source[Int]
      val signal = source.signal
      val events = new ListBuffer[Int]

      source ! (1, 2, 3)
      val listener = signal.sink.listenOnceWeak(events += _)
      source ! (4, 5, 6) // only "4" gets to the callback
      listener.unregister() // nothing happens
      source ! (7, 8, 9)

      events shouldBe List(4)
    }

    "listenOnceWeak can be unregistered before it has fired" in {
      val source = Source[Int]
      val signal = source.signal
      val events = new ListBuffer[Int]

      source ! (1, 2, 3)
      val listener = signal.sink.listenOnceWeak(events += _)
      listener.unregister()
      source ! (4, 5, 6)

      events shouldBe List()
    }

    "can have multiple listeners" in {
      val source     = Source[Int]
      val signal     = source.signal
      val events     = new ListBuffer[Int]
      val eventsOnce = new ListBuffer[Int]

      source ! (1, 2, 3)
      val listener1     = signal.sink.listen(events += _)
      val listener2     = signal.sink.listen(events += _)
      val listenerOnce1 = signal.sink.listenOnce(eventsOnce += _)
      val listenerOnce2 = signal.sink.listenOnce(eventsOnce += _)
      source ! (4, 5, 6)
      List(listener1, listener2, listenerOnce1, listenerOnce2).foreach(_.unregister())
      source ! (7, 8, 9)

      events shouldBe List(4, 4, 5, 5, 6, 6)
      eventsOnce shouldBe List(4, 4)
    }
  }

  "probe" - {
    "sample returns state's value" in {
      val source = Source[Int]
      val state  = source.signal.hold(100).probe

      state.sample shouldBe 100
      state.sample shouldBe 100

      source ! 1
      state.sample shouldBe 1
      state.sample shouldBe 1
    }

    "can have multiple probes" in {
      val source = Source[Int]
      val state  = source.signal.hold(10)
      val probe1 = state.probe
      val probe2 = state.probe

      probe1.sample shouldBe 10
      probe2.sample shouldBe 10

      source ! 1
      probe1.sample shouldBe 1
      probe2.sample shouldBe 1
    }

    "can't access state while inside transaction" in {
      val source = Source[Int]
      val state  = source.signal.hold(1).probe

      assertThrows[NewTransactionRequired.type] {
        timeline.tx { state.sample }
      }
    }

    "can't access state when in combinator functions" in {
      val source     = Source[Int]
      val state      = source.signal.hold(1)
      val stateProbe = state.probe
      val signal     = source.signal.map(_ => stateProbe.sample)
      val out        = Collector(signal)

      assertThrows[NewTransactionRequired.type] {
        source ! 1
      }

      out.events shouldBe List()
    }

    "can't access state in listener callbacks" in {
      val source = Source[Int]
      val state  = source.signal.map(_ * 2).hold(1).probe
      val events = new ListBuffer[Int]()
      source.signal.sink.listen(_ => events += state.sample)

      assertThrows[NewTransactionRequired.type] {
        source ! (1, 2, 3)
      }

      events shouldBe List()
    }
  }
}
