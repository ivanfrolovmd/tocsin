package io.tocsin.core

import io.tocsin.core.Graph.SignalNode

private[tocsin] sealed trait Signal[A]

private class SignalImpl[A](val node: SignalNode[A]) extends Signal[A]
