package io.tocsin.core

private[tocsin] sealed trait Listener

private abstract class ListenerImpl extends Listener {
  def unregister(): Unit
}
