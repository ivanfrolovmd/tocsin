package io.tocsin.scalaz

import io.tocsin.core.{
  Collector,
  CycleDetected,
  StartingTransactionInCallbacksForbidden,
  UnexpectedTransactionException
}
import org.scalatest.{FreeSpec, Matchers, OneInstancePerTest}
import io.tocsin.utils._
import scalaz.Scalaz._

import scala.Function.const
import scala.collection.mutable.ListBuffer

class UseCasesSpec extends FreeSpec with OneInstancePerTest with Matchers {
  implicit val timeline: Timeline = Timeline()

  "Signal: source -> sink" - {
    "signals sent to source appear in sink" in {
      val source = Source[String]
      val events = new ListBuffer[String]
      source.signal.sink.listen(events += _)

      source ! "hello"
      source ! "world"

      events shouldBe List("hello", "world")
    }

    "in transaction last signal wins when sent multiple times to one source" in {
      val source = Source[Int]
      val signal = source.signal
      val out    = Collector(signal)

      timeline.tx {
        for (i <- 1 to 10) {
          source ! i
        }
      }

      out.events shouldBe List(10)
    }
  }

  "State: source -> probe" - {
    "'hold' primitive should keep the last state" in {
      val source = Source[String]
      val probe  = source.signal.hold.probe

      probe.sample shouldBe ""
      source ! "hello"
      probe.sample shouldBe "hello"
      source ! "foo"
      source ! "world"
      probe.sample shouldBe "world"
    }

    "'updates' primitive should fire all updates" in {
      val source      = Source[String]
      val state       = source.signal.hold
      val probe       = state.probe
      val updatesSink = state.updates.sink
      val events      = new ListBuffer[String]()
      updatesSink.listen(events += _)

      source ! ("hello", "world")
      events shouldBe List("hello", "world")
      probe.sample shouldBe "world"
    }

    "calmed updates shouldn't fire if not changed" in {
      val source             = Source[Int]
      val signal             = source.signal
      val state              = signal.hold
      val calmedState        = state.calm
      val calmedProbe        = calmedState.probe
      val updates            = Collector(state)
      val calmedUpdates      = Collector(calmedState)
      val idempotentlyCalmed = Collector(calmedState.calm.calm.calm)

      calmedProbe.sample shouldBe 0
      source ! 10
      calmedProbe.sample shouldBe 10
      source ! 10
      calmedProbe.sample shouldBe 10
      source ! 10
      calmedProbe.sample shouldBe 10
      source ! 20
      calmedProbe.sample shouldBe 20
      source ! 20
      calmedProbe.sample shouldBe 20
      source ! 30
      calmedProbe.sample shouldBe 30
      source ! 30
      calmedProbe.sample shouldBe 30

      updates.events shouldBe List(10, 10, 10, 20, 20, 30, 30)
      calmedUpdates.events shouldBe List(10, 20, 30)
      idempotentlyCalmed.events shouldBe List(10, 20, 30)
    }

    "'updates' and 'calm' are dual" in {
      val source = Source[Int]

      val originalSignal = source.signal
      val updates        = source.signal.hold.updates
      val state1         = originalSignal.hold
      val state2         = originalSignal.hold.updates.hold

      val signalEvents  = Collector(originalSignal)
      val updatesEvents = Collector(updates)
      val state1Probe   = state1.probe
      val state2Probe   = state2.probe

      source ! (1, 2, 2, 3)

      state1Probe.sample shouldBe state2Probe.sample
      state1Probe.sample shouldBe 3

      signalEvents.events shouldBe updatesEvents.events
      signalEvents.events shouldBe List(1, 2, 2, 3)
    }
  }

  "initialization order" - {
    "ephemeral signal with snapshot on state" in {
      val source = Source[Int]

      val out = timeline.tx {
        val state = source.signal.deepen(10).hold(10).deepen(10)
        Collector(Signal.once(()).snapshot(state)((_, init) => init) <+> state.updates)
      }

      source ! (1, 2, 3)

      out.events shouldBe List(10, 1, 2, 3)
    }

    "ephemeral signal with snapshot on ephemeral state - 1" in {
      val source = Source[Int]

      val out = timeline.tx {
        val ephemeralState = Signal.always(100).deepen(10).hold(101).deepen(10)
        Collector(Signal.once(()).snapshot(ephemeralState)((_, st) => st) <+> source.signal)
      }

      source ! (1, 2, 3)
      out.events shouldBe List(100, 1, 2, 3)
    }

    "ephemeral signal with snapshot on ephemeral state - 2" in {
      val source = Source[Int]

      val out = timeline.tx {
        val ephemeralState = Signal.always(100).deepen(10).hold(101).deepen(10)
        Collector(Signal.always(()).snapshot(ephemeralState)((_, st) => st) <+> source.signal)
      }

      source ! (1, 2, 3)
      out.events shouldBe List(100, 100, 100, 100)
    }
  }

  "signal-state switch" - {
    "state containing signal can be flattened with 'switch' function" in {
      val switch  = Source[Int]
      val source1 = Source[Int]
      val source2 = Source[Int]
      val source3 = Source[Int]

      val signal1 = source1.signal
      val signal2 = source2.signal
      val signal3 = source3.signal

      val stateWithSignal: State[Signal[Int]] = switch.signal.map {
        case 1 => signal1
        case 2 => signal2
        case 3 => signal3
        case _ => Signal.never[Int]
      }.hold
      val switchedSignal: Signal[Int] = stateWithSignal.switch
      val out                         = Collector(switchedSignal)

      // black hole
      source1 ! 1
      source2 ! 2
      source3 ! 3

      // serve only from source 1
      switch ! 1
      source1 ! 4 // fire
      source2 ! 5
      source3 ! 6

      // serve only from source 2
      switch ! 2
      source1 ! 7
      source2 ! 8 // fire
      source3 ! 9

      // serve only from source 3
      switch ! 3
      source1 ! 10
      source2 ! 11
      source3 ! 12 // fire

      // black hole again
      switch ! 0
      source1 ! 13
      source2 ! 14
      source3 ! 15

      // nothing happens, because switching and firing of proxy should happen in the same tx
      out.events shouldBe List(4, 8, 12)
    }
  }

  "snapshotting" - {
    "snapshot on const" in {
      val source   = Source[Int]
      val signal   = source.signal
      val state    = State(100)
      val snapshot = signal.snapshot(state)(_ + _)
      val out      = Collector(snapshot)

      source ! (1, 2, 3)

      out.events shouldBe List(101, 102, 103)
    }

    "snapshot captures state at the beginning of the transactions" in {
      val source   = Source[Int]
      val signal   = source.signal
      val state    = signal.map(_ * 5).hold
      val snapshot = signal.snapshot(state)(_ + _)
      val out      = Collector(snapshot)

      source ! (1, 2, 3)

      out.events shouldBe List(1 + 5 * 0, 2 + 5 * 1, 3 + 5 * 2)
    }

    "state change doesn't trigger snapshot signal" in {
      val signalSource = Source[Int]
      val stateSource  = Source[Int]
      val snapshot     = signalSource.signal.snapshot(stateSource.signal.hold)(_ + _)
      val out          = Collector(snapshot)

      signalSource ! 1
      stateSource ! 100
      signalSource ! 2
      stateSource ! 200
      signalSource ! 3
      stateSource ! 300
      stateSource ! 400
      stateSource ! 500

      out.events shouldBe List(1, 102, 203)
    }
  }

  "collect" - {
    "collect by instance type" in {
      sealed trait Animal
      case class Cow(name: String) extends Animal
      case class Cat(name: String) extends Animal

      val source        = Source[Animal]
      val cats          = source.signal.collectOnly[Cat]
      val cows          = source.signal.collectOnly[Cow]
      val catsCollector = Collector(cats)
      val cowsCollector = Collector(cows)

      source ! Cat("mew")
      source ! Cow("moo")
      source ! Cat("fluffy")

      catsCollector.events shouldBe List(Cat("mew"), Cat("fluffy"))
      cowsCollector.events shouldBe List(Cow("moo"))
    }

    "map/collect by partial function" in {
      val source = Source[Int]
      val filtered = source.signal.collect {
        case 2 => "two"
        case 4 => "four"
      }
      val out = Collector(filtered)

      source ! (1, 2, 3, 4, 5)

      out.events shouldBe List("two", "four")
    }
  }

  "loops" - {
    "implement sum accumulator using recursive snapshot" in {
      val source = Source[Int]

      val sum = timeline.tx {
        lazy val signal            = source.signal.snapshot(accum)(_ + _)
        lazy val accum: State[Int] = signal.hold(100)
        accum.probe
      }

      sum.sample shouldBe 100
      source ! 1
      sum.sample shouldBe 101
      source ! 2
      sum.sample shouldBe 103
      source ! 3
      sum.sample shouldBe 106
    }

    "implement sum accumulator using 'sum' function and monoid" in {
      val source = Source[Int]

      val sum = source.signal.sum.probe

      sum.sample shouldBe 0
      source ! (1, 2, 3, 4, 5)
      sum.sample shouldBe (1 + 2 + 3 + 4 + 5)
    }

    "scan and accumulate values" in {
      val source = Source[Int]

      val scanned = Collector(source.signal.scan)

      source ! (1, 2, 3, 4, 5)
      scanned.events shouldBe List(1, 3, 6, 10, 15)
    }

    "implement value spinner" in {
      val plusButton  = Source[Unit]
      val minusButton = Source[Unit]
      val init        = Signal.once(100)

      val probe = timeline.tx {
        val sDelta                 = (plusButton.signal >| 1) <+> (minusButton.signal >| -1) <+> init
        lazy val value: State[Int] = sDelta.snapshot(value)(_ + _).hold
        value.probe
      }

      probe.sample shouldBe 100
      (1 to 2).foreach(_ => plusButton ! {})
      probe.sample shouldBe 102
      (1 to 4).foreach(_ => minusButton ! {})
      probe.sample shouldBe 98
      plusButton ! {}
      probe.sample shouldBe 99
    }

    "cycles in DAG" - {
      "signals" in {
        val source = Source[Int]
        val signal = source.signal

        lazy val self: Signal[Int] = signal >>= const(self)
        self.sink.listen(_ => {})

        assertThrows[CycleDetected.type] { source ! 1 }
      }
      "states" in {
        val source = Source[Int]
        val state  = source.signal.hold(1)

        lazy val self: State[Int] = state >>= const(self)

        assertThrows[CycleDetected.type] { self.probe }
      }
      "switch" in {
        val source = Source[Int]

        lazy val self: Signal[Int] = source.signal.map(const(self)).hold(Signal.never[Int]).switch
        self.sink.listen(_ => {})

        assertThrows[CycleDetected.type] { source ! 1 }
      }
    }
  }

  "zip" - {
    "zip with previous on value types having a zero from Monoid" in {
      val source = Source[Int]
      val zipped = Collector(source.signal.zipWithPrevious)

      source ! (10, 20, 30)

      zipped.events shouldBe List((0, 10), (10, 20), (20, 30))
    }

    "zip with previous having an explicit zero value" in {
      val source = Source[String]
      val zipped = Collector(source.signal.zipWithPrevious("start"))

      source ! ("hello", "world")

      zipped.events shouldBe List(("start", "hello"), ("hello", "world"))
    }

    "unzip a stream" in {
      val source = Source[(String, Int)]

      val (strings, ints) = source.signal.unfzip
      val stringsOut      = Collector(strings)
      val intsOut         = Collector(ints)

      source ! ("one" -> 1, "two" -> 2, "three" -> 3)

      stringsOut.events shouldBe List("one", "two", "three")
      intsOut.events shouldBe List(1, 2, 3)
    }

    "zipWithIndex" in {
      val source = Source[String]

      val out = Collector(source.signal.zipWithIndex)

      source ! ("zero", "one", "two", "three", "four", "five")

      out.events shouldBe List("zero" -> 0, "one" -> 1, "two" -> 2, "three" -> 3, "four" -> 4, "five" -> 5)
    }

    "accumulate and lazy init" in {
      val source = Source[String]
      source ! "ah"

      val mapped = source.signal.deepen(5)

      val out = Collector(mapped.snapshot(mapped.accumulate(0)((_, c) => c + 1).deepen(10))(Tuple2.apply))

      source ! ("zero", "one", "two", "three", "four", "five")

      out.events shouldBe List("zero" -> 0, "one" -> 1, "two" -> 2, "three" -> 3, "four" -> 4, "five" -> 5)
    }
  }

  "partition" in {
    val source = Source[Int]

    val (evens, odds) = source.signal.partition(_ % 2 == 0)
    val evensOut      = Collector(evens)
    val oddsOut       = Collector(odds)

    for (i <- 1 to 10) source ! i

    evensOut.events shouldBe List(2, 4, 6, 8, 10)
    oddsOut.events shouldBe List(1, 3, 5, 7, 9)
  }

  "sliding" in {
    val source = Source[Int]

    val out = Collector(source.signal.sliding(3))

    for (i <- 1 to 6) source ! i

    out.events shouldBe List(
      Seq(1),
      Seq(1, 2),
      Seq(1, 2, 3),
      Seq(2, 3, 4),
      Seq(3, 4, 5),
      Seq(4, 5, 6)
    )
  }

  "grouped" in {
    val source = Source[Int]

    val out = Collector(source.signal.grouped(3))

    for (i <- 1 to 10) source ! i

    out.events shouldBe List(
      Seq(1, 2, 3),
      Seq(4, 5, 6),
      Seq(7, 8, 9)
    )
  }

  "accumulator-related" - {
    "count should count the number of signals" in {
      val source = Source[String]

      val counter = source.signal.count.probe

      counter.sample shouldBe 0
      source ! "foo"
      counter.sample shouldBe 1
      source ! "bar"
      counter.sample shouldBe 2
      source ! ("baz", "quux")
      counter.sample shouldBe 4
    }

    "take should take only first N and then never fire" in {
      val source = Source[String]

      val out = Collector(source.signal.take(5))

      source ! ("one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten")
      out.events shouldBe List("one", "two", "three", "four", "five")
    }

    "drop should drop N elements and then fire" in {
      val source = Source[String]

      val out = Collector(source.signal.drop(5))

      source ! ("one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten")
      out.events shouldBe List("six", "seven", "eight", "nine", "ten")
    }

    "slice should fire in the range [FROM..TO)" in {
      val source = Source[String]

      val out = Collector(source.signal.slice(3, 7))

      source ! ("one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten")
      out.events shouldBe List("four", "five", "six", "seven")
    }

    "takeWhile should stop after predicate returned false" in {
      val source = Source[Int]

      val out = Collector(source.signal.takeWhile(_ < 10))

      source ! (1, 5, 3, 9, 10, 3, 1, 11, 2)

      out.events shouldBe List(1, 5, 3, 9)
    }

    "dropWhile should start after predicate returned true" in {
      val source = Source[Int]

      val out = Collector(source.signal.dropWhile(_ < 10))

      source ! (1, 5, 3, 9, 10, 3, 1, 11, 2)

      out.events shouldBe List(10, 3, 1, 11, 2)
    }
  }

  "gated signal" in {
    val signalSource = Source[Int]
    val gateSource   = Source[Boolean]

    val gate                = gateSource.signal.hold(false)
    val signal: Signal[Int] = signalSource.signal.gate(gate)
    val out                 = Collector(signal)

    signalSource ! (1, 2, 3) // skip
    gateSource ! true // open
    signalSource ! (4, 5, 6) // fire
    gateSource ! false // close
    signalSource ! (7, 8, 9) // skip
    timeline.tx {
      gateSource ! true // open
      signalSource ! 10 // skip, because at the beginning of transaction the gate was clased
    }
    signalSource ! 100 // fire

    out.events shouldBe List(4, 5, 6, 100)
  }

  "transaction management" - {
    "state should be rolled back on exception; root cause should be reported" in {
      object TheException extends Throwable

      val source = Source[Int]
      val state  = source.signal.hold(0)

      var fail = false
      state.updates.sink.listen(_ => if (fail) throw TheException else {})

      val probe = state.probe

      fail = true
      val caught1 = intercept[UnexpectedTransactionException] { source ! 100 }
      caught1.getCause shouldBe TheException
      probe.sample shouldBe 0

      fail = false
      source ! 1
      probe.sample shouldBe 1

      fail = true
      val caught2 = intercept[UnexpectedTransactionException] { timeline.tx { source ! 100 } }
      caught2.getCause shouldBe TheException
      probe.sample shouldBe 1
    }

    "starting new transaction in callback is forbidden" in {
      val source = Source[Int]

      source.signal.sink.listen { a =>
        source ! a
      }

      assertThrows[StartingTransactionInCallbacksForbidden.type] {
        source ! 2
      }
    }
  }

  "fizzbuzz" in {

    val source = Source[Int](Implicits.global)

    val signal = source.signal

    val fizz     = signal.filter(_ % 3 == 0) >| "Fizz"
    val buzz     = signal.filter(_ % 5 == 0) >| "Buzz"
    val fizzBuzz = signal.filter(_ % 15 == 0) >| "FizzBuzz"

    val result = fizzBuzz <+> fizz <+> buzz <+> signal.map(_.toString)
    val out    = Collector(result)

    for (i <- 1 to 20) { source ! i }

    out.events shouldBe List(
      "1",
      "2",
      "Fizz",
      "4",
      "Buzz",
      "Fizz",
      "7",
      "8",
      "Fizz",
      "Buzz",
      "11",
      "Fizz",
      "13",
      "14",
      "FizzBuzz",
      "16",
      "17",
      "Fizz",
      "19",
      "Buzz"
    )
  }
}
