package io.tocsin.core.memory

import java.util.Collections

import scala.collection.convert.Wrappers.JSetWrapper

class WeakSet[A] extends JSetWrapper[A](Collections.newSetFromMap(new java.util.WeakHashMap)) {
  override def empty = new WeakSet[A]
}

object WeakSet {
  def empty[A]: WeakSet[A] = new WeakSet[A]
}
