package io.tocsin.core

import io.tocsin.core.Graph.SignalEgress

private[tocsin] sealed trait Sink[A]

private class SinkImpl[A](signal: SignalImpl[A], timeline: EventTimeline) extends Sink[A] {
  def listen(cb: A => Unit, weak: Boolean, once: Boolean): ListenerImpl = {
    lazy val callback = if (!once) {
      cb
    } else { a: A =>
      cb(a)
      node.timeline.detach(node)
    }
    lazy val node: SignalEgress[A] = SignalEgress(timeline, signal.node, weak, callback)
    lazy val listener: ListenerImpl = new ListenerImpl {
      private var theNode = node // keep strong reference
      override def unregister(): Unit = synchronized {
        if (theNode == null) return
        theNode.timeline.tx { theNode.timeline.detach(theNode) }
        theNode = null // remove strong reference
      }
    }
    node.timeline.tx { node.timeline.attach(node) }
    listener
  }
}

private object Sink {
  def apply[A](signal: SignalImpl[A]): Sink[A] = new SinkImpl[A](signal, signal.node.timeline.orElseGlobal)
  def apply[A](signal: SignalImpl[A], timeline: Timeline): Sink[A] = {
    if (!timeline.isInstanceOf[EventTimeline]) throw EventTimelineRequired
    new SinkImpl[A](signal, timeline.asInstanceOf[EventTimeline])
  }
}
