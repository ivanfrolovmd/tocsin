package io.tocsin.core.memory

class Wastable[A](a: A) {
  private var theA: Any = a

  def waste: Option[A] =
    if (theA == null) None
    else
      synchronized {
        val ret = Option(theA).asInstanceOf[Option[A]]
        theA = null
        ret
      }

  def apply(): A = synchronized {
    if (theA == null)
      throw new IllegalStateException("Value already wasted")
    else
      theA.asInstanceOf[A]
  }
}

object Wastable {
  def apply[A](a: A): Wastable[A] = new Wastable(a)
}
