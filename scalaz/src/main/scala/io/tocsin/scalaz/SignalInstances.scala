package io.tocsin.scalaz

import io.tocsin.core.Primitives
import scalaz.{Applicative, Functor, MonadPlus, Monoid, Semigroup, Unzip, Zip}

trait SignalInstances extends SignalInstances0 {
  implicit val signalInstance
    : Functor[Signal] with Applicative[Signal] with MonadPlus[Signal] with Zip[Signal] with Unzip[Signal] =
    new Functor[Signal] with Applicative[Signal] with MonadPlus[Signal] with Zip[Signal] with Unzip[Signal] {

      override def point[A](a: => A): Signal[A] = Primitives.signalAlways(a)

      override def map[A, B](fa: Signal[A])(f: A => B): Signal[B] = Primitives.signalMap[A, B](fa, f)

      override def ap[A, B](fa: => Signal[A])(f: => Signal[A => B]): Signal[B] =
        Primitives.signalApply[A, A => B, B](fa, f, (a, f) => f(a))
      override def apply2[A, B, C](fa: => Signal[A], fb: => Signal[B])(f: (A, B) => C): Signal[C] =
        Primitives.signalApply[A, B, C](fa, fb, f)

      override def bind[A, B](fa: Signal[A])(f: A => Signal[B]): Signal[B] =
        Primitives.signalBind[A, B](fa, (a: A) => f(a))

      override def empty[A]: Signal[A]                               = Primitives.signalNever[A]
      override def plus[A](a: Signal[A], b: => Signal[A]): Signal[A] = Primitives.signalPlus[A](a, b)

      override def filter[A](fa: Signal[A])(f: A => Boolean): Signal[A] = Primitives.signalFilter[A](fa, f)

      override def zip[A, B](a: => Signal[A], b: => Signal[B]): Signal[(A, B)] =
        Primitives.signalApply[A, B, (A, B)](a, b, Tuple2.apply)
      override def unzip[A, B](a: Signal[(A, B)]): (Signal[A], Signal[B]) =
        (Primitives.signalMap[(A, B), A](a, _._1), Primitives.signalMap[(A, B), B](a, _._2))
    }

  implicit def SignalMonoid[A](implicit aMonoid: Monoid[A]): Monoid[Signal[A]] = new Monoid[Signal[A]] {
    override def zero: Signal[A] = Primitives.signalNever[A]
    override def append(f1: Signal[A], f2: => Signal[A]): Signal[A] =
      Primitives.signalApply[A, A, A](f1, f2, (a1: A, a2: A) => aMonoid.append(a1, a2))
  }
}

trait SignalInstances0 {
  implicit def SignalSemigroup[A](implicit aSemigroup: Semigroup[A]): Semigroup[Signal[A]] =
    new Semigroup[Signal[A]] {
      override def append(f1: Signal[A], f2: => Signal[A]): Signal[A] =
        Primitives.signalApply[A, A, A](f1, f2, (a1: A, a2: A) => aSemigroup.append(a1, a2))
    }
}
