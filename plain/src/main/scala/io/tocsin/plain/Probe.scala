package io.tocsin.plain

import io.tocsin.core
import io.tocsin.core.Primitives

class Probe[A](val probe: core.Probe[A]) {
  def sample: A = Primitives.sample[A](probe)
}
