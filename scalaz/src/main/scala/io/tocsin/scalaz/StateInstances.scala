package io.tocsin.scalaz

import io.tocsin.core.Primitives
import scalaz.{Applicative, Functor, Monad, Monoid, Semigroup}

trait StateInstances extends StateInstances0 {

  implicit val stateInstance: Functor[State] with Applicative[State] with Monad[State] =
    new Functor[State] with Applicative[State] with Monad[State] {
      override def point[A](a: => A): State[A] = Primitives.stateConst(a)

      override def map[A, B](fa: State[A])(f: A => B): State[B] = Primitives.stateMap[A, B](fa, f)

      override def ap[A, B](fa: => State[A])(f: => State[A => B]): State[B] =
        Primitives.stateApply[A, A => B, B](fa, f, (a, f) => f(a))
      override def apply2[A, B, C](fa: => State[A], fb: => State[B])(f: (A, B) => C): State[C] =
        Primitives.stateApply[A, B, C](fa, fb, f)

      override def bind[A, B](fa: State[A])(f: A => State[B]): State[B] = Primitives.stateBind[A, B](fa, (a: A) => f(a))
    }

  implicit def StateMonoid[A](implicit aMonoid: Monoid[A]): Monoid[State[A]] = new Monoid[State[A]] {
    override def zero: State[A] = Primitives.stateConst(aMonoid.zero)
    override def append(f1: State[A], f2: => State[A]): State[A] =
      Primitives.stateApply[A, A, A](f1, f2, (a1: A, a2: A) => aMonoid.append(a1, a2))
  }
}

trait StateInstances0 {
  implicit def StateSemigroup[A](implicit aSemigroup: Semigroup[A]): Semigroup[State[A]] =
    new Semigroup[State[A]] {
      override def append(f1: State[A], f2: => State[A]): State[A] =
        Primitives.stateApply[A, A, A](f1, f2, (a1: A, a2: A) => aSemigroup.append(a1, a2))
    }
}
