import org.gradle.api.*
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.kotlin.dsl.*

fun Project.setupScala() {
  group = "io.tocsin"
  setProperty("archivesBaseName", "tocsin-${project.name}_${Versions.scalaMajor}")

  apply {
    plugin("scala")
    plugin("maven")
    plugin("com.github.maiflai.scalatest")
    plugin("cz.alenkacz.gradle.scalafmt")
  }

  repositories {
    jcenter()
    mavenCentral()
  }

  dependencies {
    compile(Libs.scalaLibrary)
    compile(Libs.grizzled)
    testCompile(Libs.scalatest)
    testCompile(Libs.scalacheck)
    testRuntime(Libs.pegDown) // required for scalatest
    testRuntime(Libs.scalaXml) // required for scalatest
  }

  tasks.findByName("check")?.dependsOn("checkScalafmtAll")
}

// setting private functions here, because they are only generated if `plugin { ... }` syntax is used
private fun DependencyHandler.compile(dependencyNotation: Any): Dependency? = add("compile", dependencyNotation)
private fun DependencyHandler.testCompile(dependencyNotation: Any): Dependency? = add("testCompile", dependencyNotation)
private fun DependencyHandler.testRuntime(dependencyNotation: Any): Dependency? = add("testRuntime", dependencyNotation)
