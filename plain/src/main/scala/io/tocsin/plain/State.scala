package io.tocsin.plain

import io.tocsin.core
import io.tocsin.core.Primitives

class State[A](val state: core.State[A]) {
  def probe: Probe[A]                     = Primitives.probe[A](state)
  def probe(timeline: Timeline): Probe[A] = Primitives.probe[A](state, timeline)
  def sink: Sink[A]                       = Primitives.sink[A](Primitives.stateUpdates(state))
  def sink(timeline: Timeline): Sink[A]   = Primitives.sink[A](Primitives.stateUpdates(state), timeline)

  def map[B](f: A => B): State[B]                            = Primitives.stateMap[A, B](state, f)
  def merge[B, C](other: State[B])(f: (A, B) => C): State[C] = Primitives.stateApply[A, B, C](state, other, f)
  def tuple[B](other: State[B]): State[(A, B)]               = Primitives.stateApply[A, B, (A, B)](state, other, Tuple2.apply)
  def flatMap[B](f: A => State[B]): State[B]                 = Primitives.stateBind[A, B](state, (a: A) => f(a))

  def updates: Signal[A] = Primitives.stateUpdates(state)

  def calm: State[A]                              = Primitives.stateCalmed[A](state, java.util.Objects.equals)
  def calm(equality: (A, A) => Boolean): State[A] = Primitives.stateCalmed[A](state, equality)

  def switch[AA](implicit ev: A =:= Signal[AA]): Signal[AA] =
    Primitives.stateSignalSwitch[AA](state.map((a: A) => ev(a).signal))
}
object State {
  def apply[A](a: A): State[A] = Primitives.stateConst[A](a)
}
