package io.tocsin.scalaz

import io.tocsin.core.Collector
import org.scalatest.{FreeSpec, Matchers, OneInstancePerTest}
import scalaz.Scalaz._

/*
Edge cases that need to be fixed
 */
class FixSpec extends FreeSpec with OneInstancePerTest with Matchers {
  implicit val timeline: Timeline = Timeline()

  "Identity law in monad fails when 'once' is used" in { // return a >>= f = f a
    // TODO this works only because of optimization in SignalMonad.apply. Otherwise 'once' fires every time in the left side
    val source = Source[Int]
    val f      = (i: Int) => Signal.once(i) <+> source.signal
    val a      = 10

    val (left, right) = timeline.tx {
      val leftSide  = a.pure[Signal] >>= f
      val rightSide = f(a)
      val left      = Collector(leftSide, timeline)
      val right     = Collector(rightSide, timeline)
      (left, right)
    }

    source ! (1, 2, 3)

    left.events shouldBe right.events
    left.events shouldBe List(10, 1, 2, 3)
  }
}
