package io.tocsin.core

sealed trait TransactionException extends Throwable
case object TransactionRequired extends TransactionException {
  override def getMessage: String = "Not inside the transaction"
}
case object NewTransactionRequired extends TransactionException {
  override def getMessage: String = "New Transaction is required"
}
case class UnexpectedTransactionException(override val getCause: Throwable) extends TransactionException {
  override def getMessage: String = "Unexpected exception occurred during transaction processing"
}
case object EventTimelineRequired extends TransactionException {
  override def getMessage: String = "Operation required EventTimeline"
}
case object CycleDetected extends TransactionException {
  override def getMessage: String = "Cycle in node DAG detected, can't proceed"
}
case object StartingTransactionInCallbacksForbidden extends TransactionException {
  override def getMessage: String = "Can't start transaction in callbacks"
}
