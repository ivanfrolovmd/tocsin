package io.tocsin

import io.tocsin.core.Conversions

package object plain extends Conversions {
  implicit val timeline: Timeline <=> core.Timeline    = iso(_.timeline, new Timeline(_))
  implicit def source[A]: Source[A] <=> core.Source[A] = iso(_.source, new Source(_))
  implicit def signal[A]: Signal[A] <=> core.Signal[A] = iso(_.signal, new Signal(_))
  implicit def state[A]: State[A] <=> core.State[A]    = iso(_.state, new State(_))
  implicit def sink[A]: Sink[A] <=> core.Sink[A]       = iso(_.sink, new Sink(_))
  implicit val listener: Listener <=> core.Listener    = iso(_.listener, new Listener(_))
  implicit def probe[A]: Probe[A] <=> core.Probe[A]    = iso(_.probe, new Probe(_))
}
