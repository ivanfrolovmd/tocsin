buildscript {
  repositories {
    maven("https://plugins.gradle.org/m2/")
  }
  dependencies {
    classpath(Libs.gradleScalatest)
    classpath(Libs.gradleScalafmt)
  }
}

plugins {
  base
  idea
}
