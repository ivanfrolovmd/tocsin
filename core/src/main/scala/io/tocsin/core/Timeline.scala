package io.tocsin.core

import io.tocsin.core.Graph._
import io.tocsin.core.memory.{WeakMap, WeakSet}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

private[tocsin] sealed trait Timeline {
  def tx[Z](code: => Z): Z
  def attach(node: Node[_]): Unit
  def detach(node: Node[_]): Unit
}

private object EphemeralTimeline extends Timeline {
  override def tx[Z](code: => Z): Z        = code
  override def attach(node: Node[_]): Unit = assert(node.timeline == EphemeralTimeline)
  override def detach(node: Node[_]): Unit = {}
}

private class EventTimeline extends Timeline {
  // persistent data
  private val nodes            = new WeakSet[Node[_]]
  private val topoOrder        = new WeakMap[Node[_], Int]()
  private val children         = new WeakMap[Node[_], WeakSet[Node[_]]](WeakSet.empty)
  private val states           = new WeakMap[StateNode[_], Any]()
  private val persistedFires   = new WeakMap[SignalNode[_], Option[Any]](None)
  private val strongEgressRefs = new mutable.HashSet[SignalEgress[_]]()

  private val nodeOrdering: Ordering[Node[_]] = (a, b) => topoOrder(b) - topoOrder(a)

  // transaction-scoped data
  private val visited         = new mutable.AnyRefMap[Node[_], Boolean](_ => false)
  private val fires           = new mutable.AnyRefMap[SignalNode[_], Option[Any]](_ => None)
  private val statesAtTxStart = new mutable.AnyRefMap[StateNode[_], Any]()
  private val queue           = new mutable.PriorityQueue[Node[_]]()(nodeOrdering)
  private val initQueue       = new mutable.PriorityQueue[Node[_]]()(nodeOrdering)
  private val afterTx         = new ListBuffer[() => Unit]

  ///////////////////
  // TX Management //
  ///////////////////

  private var txStack: Int      = 0
  private var inPostTransaction = false
  override def tx[Z](code: => Z): Z = synchronized {
    if (inPostTransaction) throw StartingTransactionInCallbacksForbidden
    txStack += 1
    val ret = changeStructure { code }
    try {
      if (txStack == 1) {
        doTransaction()
      }
      ret
    } finally {
      txStack -= 1
    }
  }
  def newTx[Z](code: => Z): Z = synchronized { if (txStack > 0) throw NewTransactionRequired else tx(code) }
  /* This should ONLY be called from synchronized context */
  private def ensureInTransaction(): Unit = { if (txStack == 0) throw TransactionRequired }

  //////////////////////////
  // Structure management //
  //////////////////////////

  private var structStack: Int  = 0
  private val unorderedElements = new ListBuffer[Node[_]]
  private def changeStructure[Z](code: => Z): Z = {
    try {
      structStack += 1
      val ret = code
      if (structStack == 1 && unorderedElements.nonEmpty) {
        for (node <- unorderedElements) { reorderChildren(node, topoOrder(node)) }
        fixPriorityQueueOrder(queue)
        fixPriorityQueueOrder(initQueue)
        unorderedElements.clear()
      }
      ret
    } finally {
      structStack -= 1
    }
  }
  private def addChild(parent: Node[_], child: Node[_]): Unit = changeStructure {
    val list = children.getOrElseUpdate(parent, WeakSet.empty)
    list += child
    if (topoOrder(parent) >= topoOrder(child)) unorderedElements += parent
  }
  private def removeChild(parent: Node[_], child: Node[_]): Unit = changeStructure {
    val list = children(parent)
    list -= child
  }
  private def reorderChildren(
      parent: Node[_],
      correctOrder: Int,
      dagVisited: mutable.AnyRefMap[Node[_], Boolean] = new mutable.AnyRefMap(_ => false)): Unit = {
    if (dagVisited(parent)) throw CycleDetected else dagVisited(parent) = true
    topoOrder(parent) = correctOrder
    for (child <- getChildren(parent) if topoOrder(child) < correctOrder + 1) {
      reorderChildren(child, correctOrder + 1, dagVisited)
    }
  }
  private def fixPriorityQueueOrder(queue: mutable.PriorityQueue[Node[_]]): Unit = if (queue.nonEmpty) {
    val old = queue.toArray[Node[_]]
    queue.clear()
    queue ++= old
  }
  override def attach(node: Node[_]): Unit = synchronized {
    ensureInTransaction()
    if (nodes.contains(node)) return
    if (node.timeline != this && node.timeline != EphemeralTimeline)
      throw new IllegalArgumentException("Cross time-line is not supported")

    node match {
      // consts
      case n: SignalNever[_]  => persistedFires(n) = None
      case n: SignalOnce[_]   => for (value <- n.value.waste) { fires(n) = Some(value) }; tl.scheduleAfterTx { detach(n) }
      case n: SignalAlways[_] => persistedFires(n) = Some(n.value)
      case n: SignalEgress[_] => if (!n.weak) strongEgressRefs += n
      case n: StateConst[_]   => states(n) = n.value
      case n: StateCalmed[_]  => statesAtTxStart(n) = null
      case n: StateHold[_] =>
        if (n.timeline eq EventTimeline.this) {
          for (value <- n.zero.waste) { states(n) = value }
        } else {
          states(n) = n.zero() // don't waste value, because it comes from ephemeral timeline and might be reused
        }
      case _ => // do nothing
    }

    for (parent <- node.staticParents) { attach(parent) }

    node match {
      case n: Node[_] if n.staticParents.isEmpty => topoOrder(n) = 0
      case n: SignalEgress[_]                    => topoOrder(n) = Int.MaxValue
      case n                                     => topoOrder(n) = n.staticParents.map(topoOrder).max + 1
    }

    for (parent <- node.staticParents) { addChild(parent, node) }

    nodes += node

    if (node.isInstanceOf[StateNode[_]]
        || node.isInstanceOf[EphemeralNode[_]]
        || node.staticParents.exists(_.timeline != EphemeralTimeline)
        && !node.isInstanceOf[SignalEgress[_]]) {
      initQueue.enqueue(node)
    }
  }
  override def detach(node: Node[_]): Unit = synchronized {
    ensureInTransaction()
    node match {
      case n: SignalEgress[_] =>
        changeStructure { for (parent <- n.staticParents) { children(parent) -= n } }
        strongEgressRefs -= n
      case _ => // do nothing
    }
    node match {
      case n: StateNode[_]  => states -= n
      case n: SignalNode[_] => persistedFires -= n
    }
    nodes -= node
    topoOrder -= node
    children -= node
  }
  private[tocsin] def getChildren(parent: Node[_]): mutable.Iterable[Node[_]] = children(parent)
  private[tocsin] def getAttachedNodes: Seq[Node[_]]                          = nodes.toSeq.sortBy(topoOrder)

  ////////
  // In //
  ////////

  def ingest[A](node: SignalIngress[A], a: A): Unit = synchronized {
    ensureInTransaction()
    if (!fires.contains(node)) queue.enqueue(node)
    fires(node) = Some(a)
  }

  ///////////////
  // Mechanics //
  ///////////////

  private def doTransaction(): Unit =
    try {
      for (firedNode <- persistedFires.keysIterator) queue.enqueue(firedNode)
      traverse()
      inPostTransaction = true
      postTransaction()
    } catch {
      case e: Throwable => rollback(); throw e
    } finally {
      inPostTransaction = false
      visited.clear()
      fires.clear()
      statesAtTxStart.clear()
      queue.clear()
      initQueue.clear()
      afterTx.clear()
    }

  private val tl = new TL {
    override def fire[A](n: SignalNode[A], value: Option[A]): VisitResult = {
      fires(n) = value
      if (value.isDefined) EnqueueChildren else Noop
    }
    override def updateState[A](n: StateNode[A], value: A): VisitResult = { states(n) = value; EnqueueChildren }
    override def scheduleAfterTx(code: => Unit): Unit                   = { afterTx += (() => code) }

    override def getFired[A](signal: SignalNode[A]): Option[A] =
      persistedFires.getOrElse(signal, fires(signal)).asInstanceOf[Option[A]]
    override def state[A](state: StateNode[A]): A =
      states(state).asInstanceOf[A]
    override def stateAtStart[A](state: StateNode[A]): A =
      statesAtTxStart.getOrElse(state, states(state)).asInstanceOf[A]

    override def isChild(parent: Node[_], child: Node[_]): Boolean = children(parent).contains(child)
    override def addChild(parent: Node[_], child: Node[_]): Unit = {
      EventTimeline.this.attach(parent)
      changeStructure { EventTimeline.this.addChild(parent, child) }
    }
    override def attach(node: Node[_]): Unit = EventTimeline.this.attach(node)

    override def removeChild(parent: Node[_], child: Node[_]): Unit = changeStructure {
      EventTimeline.this.removeChild(parent, child)

    }
  }

  private def traverse(): Unit = {
    while (queue.nonEmpty || initQueue.nonEmpty) {
      val inInit   = initQueue.nonEmpty
      val theQueue = if (inInit) initQueue else queue
      val node     = theQueue.dequeue()

      if (!visited(node)) {
        node match {
          case n: StateNode[_] => for (currentState <- states.get(n)) { statesAtTxStart(n) = currentState }
          case _               => // do nothing
        }

        node.visit(tl) match {
          case EnqueueChildren =>
            visited(node) = true
            for (child <- children(node)) {
              if (child.isInstanceOf[SignalEgress[_]])
                queue.enqueue(child)
              else
                theQueue.enqueue(child)
            }
          case ReEnqueueSelf =>
            queue.enqueue(node)
          case Noop =>
            visited(node) = true
        }
      }

      if (inInit && initQueue.isEmpty) {
        visited.clear()
      }
    }
  }

  private def postTransaction(): Unit = {
    for (callback <- afterTx) {
      try {
        callback()
      } catch {
        case e: TransactionException => throw e
        case e: Throwable            => throw UnexpectedTransactionException(e)
      }
    }
  }

  private def rollback(): Unit = for (node <- statesAtTxStart.keys) { states(node) = statesAtTxStart(node) }

  /////////
  // Out //
  /////////

  def state[A](n: StateNode[A]): A = states(n).asInstanceOf[A]
}

private object Timeline {
  implicit class TimelineOps(left: Timeline) {

    /** Converge timelines */
    def ><(right: Timeline): Timeline = (left, right) match {
      case (EphemeralTimeline, EphemeralTimeline)         => EphemeralTimeline
      case (l: EventTimeline, r: EventTimeline) if l eq r => l
      case (_: EventTimeline, _: EventTimeline) =>
        throw new UnsupportedOperationException("Timeline convergence not implemented")
      case (EphemeralTimeline, r: EventTimeline) => r
      case (l: EventTimeline, EphemeralTimeline) => l
    }

    def orElseGlobal: EventTimeline = left match {
      case EphemeralTimeline => Primitives.globalTimeline.asInstanceOf[EventTimeline]
      case e: EventTimeline  => e
    }
  }
}
