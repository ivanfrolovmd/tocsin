package io.tocsin.scalaz

import io.tocsin.core.{Collector, NodeInspector}
import io.tocsin.core.memory.{WeakMap, WeakSet}
import io.tocsin.utils.gcAndExpect
import org.scalatest.{FreeSpec, Matchers, OneInstancePerTest}
import scalaz.Scalaz._

import scala.collection.mutable.ListBuffer

class MemoryManagementSpec extends FreeSpec with OneInstancePerTest with Matchers {
  implicit val timeline: Timeline = Timeline()

  "unreachable cross references in weakMap and weakSet should be garbage collected" in {
    class Data(val value: String)
    val map = new WeakMap[Data, WeakSet[Data]](WeakSet.empty)

    var obj1     = new Data("one")
    var obj2     = new Data("two")
    var obj3     = new Data("three")
    var children = new WeakSet[Data]()
    children += obj1
    children += obj2
    children += obj3

    map.put(obj1, children)
    map.put(obj2, children)
    map.put(obj3, children)

    map.size shouldBe 3
    map.values.flatten.size shouldBe 9

    obj1 = null
    obj2 = null
    obj3 = null
    children = null

    gcAndExpect { map.isEmpty }
    map shouldBe empty
  }

  "weak and strong listeners" - {
    "weak listeners stop processing callbacks when listener ref is reclaimed while strong ref continues until listener is explicitly unregistered" in {
      val source     = Source[Int]
      val signalSink = source.signal.sink

      val strongEvents = new ListBuffer[Int]
      val weakEvents   = new ListBuffer[Int]

      var strongListener = signalSink.listen(strongEvents += _)
      var weakListener   = signalSink.listenWeak(weakEvents += _)

      NodeInspector(source).egressNodes shouldBe 2
      source ! (1, 2, 3)

      strongListener = null
      weakListener = null
      gcAndExpect { NodeInspector(source).egressNodes == 1 }

      source ! (4, 5, 6)

      strongEvents shouldBe List(1, 2, 3, 4, 5, 6)
      weakEvents shouldBe List(1, 2, 3)
    }

    "'once' weak listeners stop processing callbacks when listener ref is reclaimed while strong listener persists" in {
      val source     = Source[Int]
      val signalSink = source.signal.sink

      val strongEvents = new ListBuffer[Int]
      val weakEvents   = new ListBuffer[Int]

      signalSink.listenOnce(strongEvents += _)
      signalSink.listenOnceWeak(weakEvents += _)

      gcAndExpect { NodeInspector(source).egressNodes == 1 }

      source ! (4, 5, 6)

      strongEvents shouldBe List(4)
      weakEvents shouldBe List()
    }
  }

  "gc should be able to remove branches that don't end up with listeners and/or probes" in {
    val source1 = Source[Int]
    val source2 = Source[Int]

    val signal1 = source1.signal
    val signal2 = source2.signal

    var complexState = ((signal1 |+| signal2) <+> (signal1 <+> signal2))
      .map(identity)
      .flatMap(_ => signal1)
      .hold(4) |+| State(10)
    var complexStateProbe = complexState.probe
    var complexThingSink =
      (Signal.once(1000) <+> signal1 <+> signal2 <+> Signal.always(100)).snapshot(complexState)(_ + _).scan.sink
    var weakListener = complexThingSink.listenWeak(_ => {})

    gcAndExpect { NodeInspector(source1, source2).allNodes == 19 }

    complexState = null
    complexThingSink = null
    weakListener = null
    gcAndExpect { NodeInspector(source1, source2).allNodes == 10 }

    complexStateProbe = null
    gcAndExpect { NodeInspector(source1, source2).allNodes == 2 }

    NodeInspector(source1, source2).ingressNodes shouldBe 2
  }

  "discard unused ad-hoc ephemeral nodes" in {
    val source  = Source[Int]
    val signal1 = source.signal >>= Signal.always
    val signal2 = source.signal >>= Signal.once
    val out     = Collector(signal1 |+| signal2)

    NodeInspector(source).allNodes shouldBe 7

    source ! (1, 2, 3, 4, 5)
    out.events shouldBe List(2, 4, 6, 8, 10)

    gcAndExpect { NodeInspector(source).allNodes == 7 }
    NodeInspector(source).ephemeralNodes shouldBe 0
  }

  "discard 'once' immediately after used in transaction" in {
    val source = Source[Int]
    val out = timeline.tx {
      Collector(Signal.once(100) <+> source.signal)
    }

    source ! 1

    out.events shouldBe List(100, 1)
    NodeInspector(source).onceNodes shouldBe 0
  }

  "discard value in 'once' after first use" in {
    class Data(val nr: Int)
    val source  = Source[Data]
    var value   = new Data(100)
    var weakSet = new WeakSet[Data]
    weakSet += value
    weakSet.size shouldBe 1
    val once = Signal.once(value)
    value = null
    var events = new ListBuffer[Int]

    timeline.tx { (once <+> source.signal).sink.listen(a => events += a.nr) }

    source ! new Data(1)

    events shouldBe List(100, 1)
    gcAndExpect(weakSet.isEmpty)
  }

  "don't store 'once' nodes when used in monads" in {
    val source = Source[Int]
    val out    = Collector(source.signal >>= Signal.once)
    NodeInspector(source).allNodes shouldBe 4

    source ! (1, 2, 3)

    out.events shouldBe List(1, 2, 3)
    NodeInspector(source).onceNodes shouldBe 0
    NodeInspector(source).allNodes shouldBe 4
  }

  "zero value in hold should be reclaimed when not used anymore" in {
    class Data(val nr: Int)
    val source1 = Source[Data]
    val source2 = Source[Data]
    val switch  = Source[Int]

    val reclaimableSet = new WeakSet[Data]()
    var zero1          = new Data(1)
    var zero2          = new Data(2)
    var zero3          = new Data(3)
    reclaimableSet ++= List(zero1, zero2, zero3)
    val hold1 = source1.signal.hold(zero1)
    val hold2 = source2.signal.hold(zero2)
    val hold3 = Signal.never[Data].hold(zero3)
    zero1 = null
    zero2 = null
    zero3 = null

    val switched = switch.signal.hold(1) >>= {
      case 1 => hold1
      case 2 => hold2
      case 3 => hold3
      case _ => State(new Data(0))
    }
    val probe = switched.probe

    probe.sample.nr shouldBe 1
    switch ! 1
    probe.sample.nr shouldBe 1
    switch ! 2
    probe.sample.nr shouldBe 2
    switch ! 3
    probe.sample.nr shouldBe 3
    reclaimableSet.size shouldBe 3

    source1 ! new Data(1)
    gcAndExpect(reclaimableSet.size == 2)
    reclaimableSet.map(_.nr) shouldBe Set(2, 3)
    probe.sample.nr shouldBe 3

    switch ! 2
    source2 ! new Data(2)
    gcAndExpect(reclaimableSet.size == 1)
    reclaimableSet.map(_.nr) shouldBe Set(3)
    probe.sample.nr shouldBe 2
  }
}
