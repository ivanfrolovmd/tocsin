package io.tocsin.scalaz

import io.tocsin.core.{Derivations, Primitives}
import scalaz.Monoid

import scala.reflect.ClassTag

trait SignalOps extends SignalInstances {
  implicit class CombinatorOps[A](signal: Signal[A]) {
    def hold(implicit aMonoid: Monoid[A]): State[A] = hold(aMonoid.zero)
    def hold(zero: A): State[A]                     = Primitives.stateHold[A](signal, zero)

    def snapshot[B, C](state: => State[B])(f: (A, B) => C): Signal[C] =
      Primitives.signalSnapshot[A, B, C](signal, state, f)

    def scan(implicit aMonoid: Monoid[A]): Signal[A]   = scan(aMonoid.zero)(aMonoid.append(_, _))
    def scan[B](zero: B)(work: (A, B) => B): Signal[B] = Derivations.scan[A, B](signal, zero, work)

    def sum(implicit aMonoid: Monoid[A]): State[A]          = accumulate(aMonoid.zero)(aMonoid.append(_, _))
    def accumulate[B](zero: B)(work: (A, B) => B): State[B] = Derivations.accumulate[A, B](signal, zero, work)
    def count: State[Int]                                   = Derivations.count(signal)

    def collectOnly[AA <: A](implicit cl: ClassTag[AA]): Signal[AA] = Derivations.collectOnly[A, AA](signal, cl)
    def collect[B](f: PartialFunction[A, B]): Signal[B]             = Derivations.collect[A, B](signal, f)

    def partition(predicate: A => Boolean): (Signal[A], Signal[A]) = {
      val (s1, s2) = Derivations.partition[A](signal, predicate)
      (s1, s2)
    }

    def gate(condition: => State[Boolean]): Signal[A] = Primitives.signalWithGate[A](signal, condition)

    def zipWithPrevious(implicit aMonoid: Monoid[A]): Signal[(A, A)] = zipWithPrevious(aMonoid.zero)
    def zipWithPrevious(zero: A): Signal[(A, A)]                     = Derivations.zipWithPrevious[A](signal, zero)

    def zipWithIndex: Signal[(A, Int)] = Derivations.zipWithIndex[A](signal)

    def takeWhile(predicate: A => Boolean): Signal[A] = Derivations.takeWhile[A](signal, predicate)
    def dropWhile(predicate: A => Boolean): Signal[A] = Derivations.dropWhile[A](signal, predicate)

    def take(n: Int): Signal[A]                 = Derivations.take[A](signal, n)
    def drop(n: Int): Signal[A]                 = Derivations.drop[A](signal, n)
    def slice(from: Int, until: Int): Signal[A] = Derivations.slice[A](signal, from, until)

    def sliding(n: Int): Signal[Seq[A]] = Derivations.sliding[A](signal, n)
    def grouped(n: Int): Signal[Seq[A]] = Derivations.grouped[A](signal, n)
  }

  implicit class EgressOps[A](signal: Signal[A]) {
    def sink: Sink[A]                     = Primitives.sink[A](signal)
    def sink(timeline: Timeline): Sink[A] = Primitives.sink[A](signal, timeline)
  }
}
