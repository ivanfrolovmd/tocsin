package io.tocsin.scalaz

import io.tocsin.core.{Collector, NodeInspector}
import org.scalatest.{FreeSpec, Matchers, OneInstancePerTest}
import scalaz.Scalaz._

//noinspection ScalaUnusedSymbol
class OptimalGraphStructureSpec extends FreeSpec with OneInstancePerTest with Matchers {
  implicit val timeline: Timeline = Timeline()

  "multiple calls to 'signal' doesn't produce multiple nodes" in {
    val source  = Source[Int]
    val signal1 = source.signal
    val signal2 = source.signal
    val signal3 = source.signal
    NodeInspector(source).allNodes shouldBe 1
  }

  "calling 'calm' multiple times on the same state shouldn't result in multiple Calmed nodes" in {
    val source = Source[Int]
    val thing  = source.signal.hold(0).calm.calm.calm.calm
    val probe  = thing.probe
    NodeInspector(source).calmNodes shouldBe 1
  }

  "calling hold/updates multiple times on the same state shoulnd't result in multiple nodes" in {
    val source = Source[Int]
    val thing  = source.signal.hold(0).map(_ + 1).updates.hold(1).updates.hold(2).updates
    val out    = Collector(thing)
    source ! (1, 2, 3)
    out.events shouldBe List(2, 3, 4)
    NodeInspector(source).updatesNodes shouldBe 1
  }

  "no new StateUpdates nodes should be created for redundant case" in {
    val source = Source[Int]
    val thing  = source.signal.hold(0).updates.hold(1).updates.hold(2).updates
    val out    = Collector(thing)
    source ! (1, 2, 3)
    out.events shouldBe List(1, 2, 3)
    NodeInspector(source).updatesNodes shouldBe 0
  }

  "semigroup appending produces just one applicative node" in {
    val source = Source[Int]
    val thing  = source.signal |+| source.signal
    thing.sink.listen(_ => {})
    NodeInspector(source).signalApplyNodes shouldBe 1
  }
}
