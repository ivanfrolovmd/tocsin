# Tocsin: Functional Reactive library for Scala

## Installation instructions

Add to gradle:
```
repositories {
  maven("https://jitpack.io")
}

dependencies {
  implementation("com.gitlab.ivanfrolovmd:tocsin-scalaz_2.12:0.0.5")
}
```

Add to sbt:
```
resolvers += "jitpack" at "https://jitpack.io"
libraryDependencies += "com.gitlab.ivanfrolovmd" %% "tocsin-scalaz" % "0.0.5"
```

## Existing bindings:

* `tocsin-scalaz` with scalaz bindings
* `tocsin-plain` with plain interfaces

## Work in progress

This is work in progress, documentation and usage guides coming soon.

Meanwhile, take a look at some [Usage Examples](https://gitlab.com/ivanfrolovmd/tocsin/wikis/Tutorials/Usage-Examples)
