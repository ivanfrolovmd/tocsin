package io.tocsin.plain

import io.tocsin.core
import io.tocsin.core.Primitives

class Listener(val listener: core.Listener) {
  def unregister(): Unit = Primitives.unregister(listener)
}
