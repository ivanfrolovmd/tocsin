package io.tocsin.scalaz

import io.tocsin.core.Collector
import io.tocsin.utils._
import org.scalatest.{FreeSpec, Matchers, OneInstancePerTest}
import scalaz.Scalaz._

import scala.Function.const
import scala.collection.mutable.ListBuffer

class EphemeralNodesSpec extends FreeSpec with OneInstancePerTest with Matchers {
  "global" - {
    "probe on const" in {
      State(10).map(_ * 2).probe.sample shouldBe 20
    }

    "listener on 'always' fires always" in {
      val events = new ListBuffer[Int]()

      val signal = Signal.always(10).map(_ * 2)
      signal.sink.listen(events += _)
      signal.sink.listen(_ => {}) // forces transaction
      signal.sink.listen(_ => {}) // forces transaction

      events shouldBe List(20, 20, 20)
    }

    "listener on 'once' fires only once" in {
      val events = new ListBuffer[Int]()

      val signal       = Signal.once(10)
      val signalMapped = Signal.once(10).map(_ * 2)
      signal.sink.listen(events += _)
      signalMapped.sink.listen(events += _)
      signal.sink.listen(_ => {})       // forces transaction
      signalMapped.sink.listen(_ => {}) // forces transaction
      signal.sink.listen(_ => {})       // forces transaction
      signalMapped.sink.listen(_ => {}) // forces transaction

      events shouldBe List(10, 20)
    }

    "listenOnce on 'once'" in {
      Signal.once(10).map(_ * 2).sink.listenOnce(a => a shouldBe 20)
    }

    "'never' never fires" in {
      val events = new ListBuffer[Int]()

      val signal = Signal.never[Int]
      signal.sink.listen(events += _)
      signal.sink.listen(_ => {}) // forces transaction
      signal.sink.listen(_ => {}) // forces transaction

      events shouldBe List()
    }

    "combinations" - {
      "once plus always" in {
        val sink   = (Signal.once(1) <+> Signal.always(2)).sink
        val events = new ListBuffer[Int]
        sink.listen(events += _)
        sink.listen(_ => {}) // forces transaction
        sink.listen(_ => {}) // forces transaction

        events shouldBe List(1, 2, 2)
      }

      "hold once plus always" in {
        val state = (Signal.once(1).map(identity) <+> Signal.always(2).map(identity)).hold(3).probe
        state.sample shouldBe 1
        state.sample shouldBe 2
        state.sample shouldBe 2
        state.sample shouldBe 2
      }

      "hold on never" in {
        val state = Signal.never[Int].hold(5).probe
        state.sample shouldBe 5
        state.sample shouldBe 5
      }
    }
  }

  "attaching to event timeline" - {
    implicit val timeline: Timeline = Timeline()

    "repeated usage of Signal.once results only in one fire - 'once' instance can't be reused" in {
      val source    = Source[Unit]
      val once      = Signal.once("boom")
      val out       = source.signal >>= const(once)
      val collector = Collector(out)

      source ! ({}, {}, {})

      collector.events shouldBe List("boom")
    }

    "hold state variance with hold, once and always" in {
      val source = Source[Unit]
      val once   = Signal.once(1)
      val always = Signal.always(2)
      val hold   = (once <+> always).hold

      val collector = timeline.tx {
        val out = source.signal.snapshot(hold)((_, b) => b)
        source ! {}
        Collector(out)
      }

      source ! ({}, {}, {})

      collector.events shouldBe List(0, 1, 2, 2)
    }

    "merge with 2nd level ephemeral applicative" in {
      val source    = Source[Int]
      val ingress   = source.signal
      val out       = ingress |+| (10.pure[Signal] |+| 100.pure[Signal])
      val collector = Collector(out)

      source ! (1, 2, 3)

      collector.events shouldBe List(111, 112, 113)
    }

    "merge with 2nd level ephemeral plus" in {
      val source      = Source[Int]
      val independent = Source[Int]
      val ingress     = source.signal
      val collector = timeline.tx {
        val out = ingress <+> (Signal.once(1000).deepen(5) <+> Signal.always(10))
        Collector(out)
      }

      source ! 1
      independent ! 2
      source ! 3
      independent ! 4

      collector.events shouldBe List(1000, 1, 10, 3, 10)
    }

    "snapshot on deep const" in {
      val source    = Source[Int]
      val signal    = source.signal
      val state     = State(100).deepen(5)
      val snapshot  = signal.snapshot(state)(_ + _)
      val collector = Collector(snapshot)

      source ! (1, 2, 3)
      collector.events shouldBe List(101, 102, 103)
    }

    "flatmap on deep signals" in {
      val switch  = Source[Int]
      val source1 = Source[Int]
      val source2 = Source[Int]

      val signal1 = source1.signal.deepen(1)
      val signal2 = source2.signal.deepen(5)
      val signal3 = Signal.always(100).deepen(5)

      val switchSignal = switch.signal >>= {
        case 1 => signal1
        case 2 => signal2
        case 3 => signal3
        case _ => Signal.never[Int]
      }

      val collector = new Collector(switchSignal.sink)

      // black hole
      timeline.tx {
        source1 ! 1
        source2 ! 2
      }

      timeline.tx {
        switch ! 1
        source1 ! 4 // should fire
        source2 ! 5
      }

      timeline.tx {
        switch ! 2
        source1 ! 7
        source2 ! 8 // should fire
      }

      // serve only from source 3
      timeline.tx {
        switch ! 3
        source1 ! 10
        source2 ! 11
      }

      // black hole again
      timeline.tx {
        switch ! 0
        source1 ! 13
        source2 ! 14
      }

      collector.events shouldBe List(4, 8, 100)
    }

    "signal applicative with deep ephemeral" in {
      val source = Source[Int]
      val signal = source.signal

      val leftEphemeral  = 100.pure[Signal].deepen(5) |+| signal
      val rightEphemeral = signal |+| 100.pure[Signal].deepen(5)

      val leftCollector  = new Collector(leftEphemeral.sink)
      val rightCollector = new Collector(rightEphemeral.sink)

      source ! 10

      leftCollector.events shouldBe List(110)
      rightCollector.events shouldBe List(110)
    }

    "signal monoid with deep ephemerals" in {
      val source = Source[Int]
      val signal = source.signal

      val (left, right) = timeline.tx {
        val leftEphemeral  = 100.pure[Signal].deepen(5) <+> signal
        val rightEphemeral = signal <+> 100.pure[Signal].deepen(5)
        source ! 1
        (new Collector(leftEphemeral.sink), new Collector(rightEphemeral.sink))
      }

      source ! (2, 3)

      left.events shouldBe List(100, 100, 100)
      right.events shouldBe List(1, 2, 3)
    }

    "state applicative with deep ephemeral" in {
      val source = Source[Int]
      val state  = source.signal.hold(0)
      val probe  = (state |+| (State(10).deepen(5) |+| State(100))).probe

      probe.sample shouldBe 110
      source ! 1
      probe.sample shouldBe 111
      source ! 2
      probe.sample shouldBe 112
    }
  }
}
