package io.tocsin.core

import io.tocsin.core.Graph.SignalIngress

private[tocsin] sealed trait Source[A]

private class SourceImpl[A](val timeline: EventTimeline) extends Source[A] {
  private val node               = SignalIngress[A](timeline)
  val signal: Signal[A]          = Attach(node)
  def send[AA <: A](a: AA): Unit = node.timeline.tx { node.timeline.ingest(node, a) }
}

private object Source {
  def apply[A](implicit timeline: Timeline): Source[A] = {
    if (!timeline.isInstanceOf[EventTimeline]) throw EventTimelineRequired
    new SourceImpl[A](timeline.asInstanceOf[EventTimeline])
  }
}
