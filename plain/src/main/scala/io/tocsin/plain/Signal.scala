package io.tocsin.plain

import io.tocsin.core
import io.tocsin.core.Primitives

class Signal[A](val signal: core.Signal[A]) {
  def sink: Sink[A]                     = Primitives.sink[A](signal)
  def sink(timeline: Timeline): Sink[A] = Primitives.sink[A](signal, timeline)

  def hold(zero: A): State[A] = Primitives.stateHold(signal, zero)

  def map[B](f: A => B): Signal[B]                             = Primitives.signalMap[A, B](signal, f)
  def merge[B, C](other: Signal[B])(f: (A, B) => C): Signal[C] = Primitives.signalApply[A, B, C](signal, other, f)
  def zip[B](other: Signal[B]): Signal[(A, B)]                 = Primitives.signalApply[A, B, (A, B)](signal, other, Tuple2.apply)
  def orElse(other: Signal[A]): Signal[A]                      = Primitives.signalPlus[A](signal, other)
  def ||(other: Signal[A]): Signal[A]                          = orElse(other)
  def filter(p: A => Boolean): Signal[A]                       = Primitives.signalFilter[A](signal, p)
  def flatMap[B](f: A => Signal[B]): Signal[B]                 = Primitives.signalBind[A, B](signal, (a: A) => f(a))

  def gate(gate: State[Boolean]): Signal[A]                      = Primitives.signalWithGate[A](signal, gate)
  def snapshot[B, C](state: State[B])(f: (A, B) => C): Signal[C] = Primitives.signalSnapshot[A, B, C](signal, state, f)
}
object Signal {
  def never[A]: Signal[A]            = Primitives.signalNever[A]
  def once[A](value: A): Signal[A]   = Primitives.signalOnce[A](value)
  def always[A](value: A): Signal[A] = Primitives.signalAlways[A](value)
}
