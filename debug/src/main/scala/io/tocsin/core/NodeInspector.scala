package io.tocsin.core

import io.tocsin.core.Graph._

import scala.reflect.ClassTag

class NodeInspector(timeline: EventTimeline) {
  private lazy val attachedNodes: Seq[Node[_]] = timeline.getAttachedNodes

  lazy val allNodes: Int         = attachedNodes.size
  lazy val ingressNodes: Int     = nodesOf[SignalIngress[_]].size
  lazy val egressNodes: Int      = nodesOf[SignalEgress[_]].size
  lazy val stateNodes: Int       = nodesOf[StateNode[_]].size
  lazy val signalNodes: Int      = nodesOf[SignalNode[_]].size
  lazy val ephemeralNodes: Int   = nodesOf[EphemeralNode[_]].size
  lazy val onceNodes: Int        = nodesOf[SignalOnce[_]].size
  lazy val calmNodes: Int        = nodesOf[StateCalmed[_]].size
  lazy val updatesNodes: Int     = nodesOf[SignalStateUpdates[_]].size
  lazy val signalApplyNodes: Int = nodesOf[SignalApply[_, _, _]].size

  private def nodesOf[T](implicit cl: ClassTag[T]): Seq[T] =
    attachedNodes.filter(n => cl.runtimeClass.isInstance(n)).asInstanceOf[Seq[T]]

  def prettyStructure: String =
    attachedNodes
      .map { node =>
        val prefix = node match {
          case _: SignalIngress[_] => "→"
          case _: SignalEgress[_]  => "←"
          case _                   => " "
        }
        s"$prefix ${nodeName(node)}: ${timeline.getChildren(node).map(nodeName).mkString("[", ", ", "]")}"
      }
      .mkString("\n")
  def prettyPrint(): Unit = println(prettyStructure)

  private def nodeName(node: Node[_]) = {
    val name          = node.getClass.getSimpleName
    val hash          = Integer.toUnsignedString(node.hashCode(), Character.MAX_RADIX).takeRight(4)
    val ephemeralStar = if (node.timeline == EphemeralTimeline) "*" else ""
    s"${name}_$hash$ephemeralStar"
  }
}

object NodeInspector {
  def apply(sources: Source[_]*): NodeInspector = {
    val timelines =
      sources.map(a => a.asInstanceOf[SourceImpl[_]].signal.asInstanceOf[SignalImpl[_]].node.timeline).distinct
    assert(timelines.size == 1)
    assert(timelines.head.isInstanceOf[EventTimeline])
    new NodeInspector(timelines.head.asInstanceOf[EventTimeline])
  }
}
