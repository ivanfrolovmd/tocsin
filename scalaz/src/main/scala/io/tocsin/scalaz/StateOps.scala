package io.tocsin.scalaz

import io.tocsin.core.Primitives

trait StateOps {
  implicit class CombinatorOps[A](state: State[A]) {
    def updates: Signal[A] = Primitives.stateUpdates[A](state)

    def calm: State[A]                                   = calm(java.util.Objects.equals)
    def calm(equalityCheck: (A, A) => Boolean): State[A] = Primitives.stateCalmed[A](state, equalityCheck)
  }

  implicit class SignalSwitchOps[A](state: State[Signal[A]]) {
    def switch: Signal[A] = Primitives.stateSignalSwitch[A](Primitives.stateMap(state, (s: Signal[A]) => s.signal))
  }

  implicit class EgressOps[A](state: State[A]) extends SignalOps {
    def probe: Probe[A]                     = Primitives.probe[A](state)
    def probe(timeline: Timeline): Probe[A] = Primitives.probe[A](state, timeline)

    def sink: Sink[A]                     = Primitives.sink[A](Primitives.stateUpdates(state))
    def sink(timeline: Timeline): Sink[A] = Primitives.sink[A](Primitives.stateUpdates(state), timeline)
  }
}
