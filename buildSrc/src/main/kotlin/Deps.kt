@file:Suppress("SpellCheckingInspection", "EnumEntryName", "MayBeConstant")

import org.gradle.api.*
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.kotlin.dsl.*

enum class Projects {
  core,
  debug,
  plain,
  scalaz,
  ;

  fun path(): String = name // Refactor when hierarchy is introduced
  fun dependencyName(): String = ":$name"
}

object Versions {
  val scala = "2.12.8"
  val scalaMajor = scala.split(".").run { take(size - 1) }.joinToString(".")
  val scalaXml = "1.1.1"
  val scalaz = "7.2.27"
  val scalactic = "3.0.5"
  val scalacheck = "1.14.0"
  val pegDown = "1.4.2"
  val grizzled = "1.3.2"

  val gradleScalatest = "0.25"
  val gradleScalafmt = "1.8.0"
}

object Libs {
  val scalaLibrary = "org.scala-lang:scala-library:${Versions.scala}"
  val scalaz = scala("org.scalaz", "scalaz-core", Versions.scalaz)
  val scalazConcurrent = scala("org.scalaz", "scalaz-concurrent", Versions.scalaz)
  val grizzled = scala("org.clapper", "grizzled-slf4j", Versions.grizzled)

  // testing
  val scalactic = scala("org.scalactic", "scalactic", Versions.scalactic)
  val scalatest = scala("org.scalatest", "scalatest", Versions.scalactic)
  val pegDown = "org.pegdown:pegdown:${Versions.pegDown}"
  val scalacheck = scala("org.scalacheck", "scalacheck", Versions.scalacheck)
  val scalaXml = scala("org.scala-lang.modules","scala-xml",Versions.scalaXml)

  // gradle plugins
  val gradleScalatest = "gradle.plugin.com.github.maiflai:gradle-scalatest:${Versions.gradleScalatest}"
  val gradleScalafmt = "gradle.plugin.cz.alenkacz:gradle-scalafmt:${Versions.gradleScalafmt}"
}

fun DependencyHandler.compile(p: Projects) = add("compile", project(p.dependencyName()))
fun DependencyHandler.compileOnly(p: Projects) = add("compileOnly", project(p.dependencyName()))
fun DependencyHandler.testCompile(p: Projects) = add("testCompile", project(p.dependencyName()))
fun DependencyHandler.testCompileOnly(p: Projects) = add("testCompileOnly", project(p.dependencyName()))
fun DependencyHandler.runtime(p: Projects) = add("runtime", project(p.dependencyName()))
fun DependencyHandler.runtimeOnly(p: Projects) = add("runtimeOnly", project(p.dependencyName()))

private fun scala(group: String, artifact: String, version: String) = "$group:${artifact}_${Versions.scalaMajor}:$version"
