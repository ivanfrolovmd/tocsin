package io.tocsin.core

import io.tocsin.core.memory.Wastable

private object Graph {

  sealed trait Node[A] {

    /** Timeline this node belongs to: Ephemeral or Real */
    val timeline: Timeline

    /** Static parents, which notify this node on change */
    val staticParents: Seq[Node[_]]

    /** Traversal visiting method */
    def visit(tl: TL): VisitResult
  }

  sealed trait SignalNode[A] extends Node[A]
  sealed trait StateNode[A]  extends Node[A]

  class SignalIngress[A] private (val timeline: EventTimeline) extends SignalNode[A] {
    override val staticParents: Seq[Node[_]] = Seq.empty
    override def visit(tl: TL): VisitResult  = EnqueueChildren
  }
  object SignalIngress {
    def apply[A](timeline: EventTimeline): SignalIngress[A] = new SignalIngress(timeline.asInstanceOf[EventTimeline])
  }

  class SignalMap[A, B] private (val timeline: Timeline, val parent: SignalNode[A], val f: A => B)
      extends SignalNode[B] {
    override val staticParents: Seq[SignalNode[A]] = Seq(parent)
    override def visit(tl: TL): VisitResult        = tl.fire(this, tl.getFired(parent).map(f))
  }
  object SignalMap {
    def apply[A, B](parent: SignalNode[A], f: A => B): SignalNode[B] = parent match {
      case _: SignalNever[A]  => SignalNever()
      case s: SignalOnce[A]   => SignalOnce(f(s.value()))
      case s: SignalAlways[A] => SignalAlways(f(s.value))
      case s: SignalMap[_, A] => new SignalMap(s.timeline, s.parent, f compose s.f)
      case s                  => new SignalMap(s.timeline, s, f)
    }
  }

  class SignalApply[A, B, C] private (val timeline: Timeline, fa: SignalNode[A], fb: SignalNode[B], f: (A, B) => C)
      extends SignalNode[C] {
    override val staticParents: Seq[Node[_]] = Seq(fa, fb)
    override def visit(tl: TL): VisitResult = {
      val value = for {
        a <- tl.getFired(fa)
        b <- tl.getFired(fb)
      } yield f(a, b)
      tl.fire(this, value)
    }
  }
  object SignalApply {
    def apply[A, B, C](fa: SignalNode[A], fb: SignalNode[B], f: (A, B) => C): SignalNode[C] = (fa, fb) match {
      case (_: SignalNever[A], _)                   => SignalNever()
      case (_, _: SignalNever[B])                   => SignalNever()
      case (a: SignalAlways[A], b: SignalOnce[B])   => SignalOnce(f(a.value, b.value()))
      case (a: SignalOnce[A], b: SignalAlways[B])   => SignalOnce(f(a.value(), b.value))
      case (a: SignalAlways[A], b: SignalAlways[B]) => SignalAlways(f(a.value, b.value))
      case (a: SignalAlways[A], b: SignalNode[B])   => SignalMap(b, b => f(a.value, b))
      case (a: SignalNode[A], b: SignalAlways[B])   => SignalMap(a, a => f(a, b.value))
      case (a, b)                                   => new SignalApply(a.timeline >< b.timeline, a, b, f)
    }
  }

  class SignalPlus[A] private (val timeline: Timeline, left: SignalNode[A], right: SignalNode[A])
      extends SignalNode[A] {
    override val staticParents: Seq[Node[_]] = Seq(left, right)
    override def visit(tl: TL): VisitResult  = tl.fire(this, tl.getFired(left) orElse tl.getFired(right))
  }
  object SignalPlus {
    def apply[A](left: SignalNode[A], right: SignalNode[A]): SignalNode[A] = (left, right) match {
      case (_: SignalNever[A], r: SignalNode[A]) => r
      case (l: SignalNode[A], _: SignalNever[A]) => l
      case (l: SignalNode[A], r: SignalNode[A])  => new SignalPlus(l.timeline >< r.timeline, l, r)
    }
  }

  class SignalFilter[A] private (val timeline: Timeline, val parent: SignalNode[A], val predicate: A => Boolean)
      extends SignalNode[A] {
    override val staticParents: Seq[Node[_]] = Seq(parent)
    override def visit(tl: TL): VisitResult  = tl.fire(this, tl.getFired(parent).filter(predicate))
  }
  object SignalFilter {
    def apply[A](parent: SignalNode[A], predicate: A => Boolean): SignalNode[A] = parent match {
      case s: SignalFilter[A] => new SignalFilter[A](parent.timeline, s.parent, a => s.predicate(a) && predicate(a))
      case s: SignalNever[A]  => s
      case s: SignalOnce[A]   => if (predicate(s.value())) s else SignalNever()
      case s: SignalAlways[A] => if (predicate(s.value)) s else SignalNever()
      case s                  => new SignalFilter(s.timeline, s, predicate)
    }
  }

  class SignalMonad[A] private (val timeline: Timeline, parent: SignalNode[SignalNode[A]]) extends SignalNode[A] {
    override val staticParents: Seq[Node[_]] = Seq(parent)
    def visit(tl: TL): VisitResult =
      tl.getFired(parent).fold[VisitResult](Noop) { proxySignal =>
        if (tl.isChild(proxySignal, this)) {
          tl.fire(this, tl.getFired(proxySignal))
        } else {
          tl.addChild(proxySignal, this)
          tl.scheduleAfterTx(tl.removeChild(proxySignal, this))
          ReEnqueueSelf
        }
      }
  }
  object SignalMonad {
    def apply[A](parent: SignalNode[SignalNode[A]]): SignalNode[A] = parent match {
      case _: SignalNever[SignalNode[A]]  => SignalNever()
      case s: SignalAlways[SignalNode[A]] => s.value
      case s                              => new SignalMonad[A](s.timeline, s)
    }
  }

  class SignalSwitch[A] private (val timeline: Timeline, parent: StateNode[SignalNode[A]]) extends SignalNode[A] {
    override val staticParents: Seq[Node[_]] = Seq(parent)
    override def visit(tl: TL): VisitResult = {
      val oldSignal = tl.stateAtStart(parent)
      val newSignal = tl.state(parent)
      if (oldSignal ne newSignal) {
        tl.removeChild(oldSignal, this)
        tl.addChild(newSignal, this)
      }
      tl.fire(this, tl.getFired(oldSignal))
    }
  }
  object SignalSwitch {
    def apply[A](parent: StateNode[SignalNode[A]]): SignalNode[A] = parent match {
      case s: StateConst[SignalNode[A]] => s.value
      case s                            => new SignalSwitch[A](s.timeline, s)
    }
  }

  class SignalWithGate[A] private (val timeline: Timeline, signal: SignalNode[A], gate: => StateNode[Boolean])
      extends SignalNode[A] {
    override val staticParents: Seq[Node[_]] = Seq(signal)
    private lazy val theGate                 = gate
    private var initialized                  = false

    override def visit(tl: TL): VisitResult =
      if (initialized) {
        if (tl.stateAtStart(theGate)) tl.fire(this, tl.getFired(signal)) else Noop
      } else {
        tl.attach(theGate)
        initialized = true
        ReEnqueueSelf
      }
  }
  object SignalWithGate {
    def apply[A](signal: SignalNode[A], gate: => StateNode[Boolean]): SignalNode[A] = signal match {
      case _: SignalNever[A] => SignalNever()
      case s                 => new SignalWithGate[A](s.timeline, s, gate)
    }
  }

  class SignalSnapshot[A, B, C] private (val timeline: Timeline,
                                         signal: SignalNode[A],
                                         state: => StateNode[B],
                                         f: (A, B) => C)
      extends SignalNode[C] {
    override val staticParents: Seq[Node[_]] = Seq(signal)
    private lazy val theState                = state
    private var initialized                  = false

    override def visit(tl: TL): VisitResult =
      if (initialized) {
        tl.fire(this, tl.getFired(signal).map(a => f(a, tl.stateAtStart(theState))))
      } else {
        tl.attach(theState)
        initialized = true
        ReEnqueueSelf
      }
  }
  object SignalSnapshot {
    def apply[A, B, C](signal: SignalNode[A], state: => StateNode[B], f: (A, B) => C): SignalNode[C] = signal match {
      case _: SignalNever[A] => SignalNever()
      case s                 => new SignalSnapshot(s.timeline, s, state, f)
    }
  }

  class SignalStateUpdates[A] private (val timeline: Timeline, state: StateNode[A]) extends SignalNode[A] {
    override val staticParents: Seq[Node[_]] = Seq(state)
    override def visit(tl: TL): VisitResult  = tl.fire(this, Some(tl.state(state)))
  }
  object SignalStateUpdates {
    def apply[A](state: StateNode[A]): SignalNode[A] = state match {
      case s: StateHold[A]                    => s.signal
      case ExistingSignalStateUpdates(signal) => signal
      case s                                  => new SignalStateUpdates(s.timeline, s)
    }
  }

  class SignalEgress[A] private (val timeline: EventTimeline,
                                 val weak: Boolean,
                                 parent: SignalNode[A],
                                 callback: A => Unit)
      extends SignalNode[A] {
    override val staticParents: Seq[Node[_]] = Seq(parent)
    override def visit(tl: TL): VisitResult = {
      for (firedValue <- tl.getFired(parent)) { tl.scheduleAfterTx(callback(firedValue)) }
      Noop
    }
  }
  object SignalEgress {
    def apply[A](timeline: EventTimeline, parent: SignalNode[A], weak: Boolean, callback: A => Unit): SignalEgress[A] =
      new SignalEgress(timeline, weak, parent, callback)
  }

  class StateHold[A] private (val timeline: Timeline, val signal: SignalNode[A], val zero: Wastable[A])
      extends StateNode[A] {
    override val staticParents: Seq[Node[_]] = Seq(signal)
    override def visit(tl: TL): VisitResult  = tl.getFired(signal).fold[VisitResult](Noop)(v => tl.updateState(this, v))
  }
  object StateHold {
    def apply[A](s: SignalNode[A], zero: A): StateNode[A] = new StateHold[A](s.timeline, s, Wastable(zero))
  }

  class StateCalmed[A] private (val timeline: Timeline, parent: StateNode[A], val equalityCheck: (A, A) => Boolean)
      extends StateNode[A] {
    override val staticParents: Seq[Node[_]] = Seq(parent)
    override def visit(tl: TL): VisitResult =
      if (!equalityCheck(tl.stateAtStart(this), tl.state(parent))) tl.updateState(this, tl.state(parent)) else Noop
  }
  object StateCalmed {
    def apply[A](parent: StateNode[A], equalityCheck: (A, A) => Boolean): StateNode[A] = parent match {
      case s: StateCalmed[A] if s.equalityCheck eq equalityCheck => s
      case s                                                     => new StateCalmed(s.timeline, s, equalityCheck)
    }
  }

  class StateMap[A, B] private (val timeline: Timeline, val parent: StateNode[A], val f: A => B) extends StateNode[B] {
    override val staticParents: Seq[Node[_]] = Seq(parent)
    override def visit(tl: TL): VisitResult  = tl.updateState(this, f(tl.state(parent)))
  }
  object StateMap {
    def apply[A, B](parent: StateNode[A], f: A => B): StateNode[B] = parent match {
      case s: StateMap[_, A] => new StateMap(s.timeline, s.parent, f compose s.f)
      case s: StateConst[A]  => StateConst(f(s.value))
      case s                 => new StateMap(s.timeline, s, f)
    }
  }

  class StateApply[A, B, C] private (val timeline: Timeline, fa: StateNode[A], fb: StateNode[B], f: (A, B) => C)
      extends StateNode[C] {
    override val staticParents: Seq[Node[_]] = Seq(fa, fb)
    override def visit(tl: TL): VisitResult  = tl.updateState(this, f(tl.state(fa), tl.state(fb)))
  }
  object StateApply {
    def apply[A, B, C](fa: StateNode[A], fb: StateNode[B], f: (A, B) => C): StateNode[C] =
      new StateApply(fa.timeline >< fb.timeline, fa, fb, f)
  }

  class StateMonad[A] private (val timeline: Timeline, parent: StateNode[StateNode[A]]) extends StateNode[A] {
    override val staticParents: Seq[Node[_]] = Seq(parent)
    override def visit(tl: TL): VisitResult =
      if (tl.isChild(tl.state(parent), this)) {
        tl.updateState(this, tl.state(tl.state(parent)))
      } else {
        tl.removeChild(tl.stateAtStart(parent), this)
        tl.addChild(tl.state(parent), this)
        ReEnqueueSelf
      }
  }
  object StateMonad {
    def apply[A](parent: StateNode[StateNode[A]]): StateNode[A] = parent match {
      case s: StateConst[StateNode[A]] => s.value
      case s                           => new StateMonad[A](s.timeline, s)
    }
  }

  sealed trait EphemeralNode[A] extends Node[A] {
    override val timeline: Timeline          = EphemeralTimeline
    override val staticParents: Seq[Node[_]] = Seq.empty
    override def visit(tl: TL): VisitResult  = EnqueueChildren
  }
  class SignalNever[A] private ()                      extends SignalNode[A] with EphemeralNode[A]
  class SignalOnce[A] private (val value: Wastable[A]) extends SignalNode[A] with EphemeralNode[A]
  class SignalAlways[A] private (val value: A)         extends SignalNode[A] with EphemeralNode[A]
  class StateConst[A] private (val value: A)           extends StateNode[A] with EphemeralNode[A]
  object SignalNever  { def apply[A](): SignalNever[A]          = new SignalNever()               }
  object SignalOnce   { def apply[A](value: A)                  = new SignalOnce(Wastable(value)) }
  object SignalAlways { def apply[A](value: A): SignalAlways[A] = new SignalAlways(value)         }
  object StateConst   { def apply[A](value: A): StateConst[A]   = new StateConst(value)           }

  /** Timeline ops callback */
  trait TL {
    // ops
    def fire[A](signal: SignalNode[A], value: Option[A]): VisitResult
    def updateState[A](state: StateNode[A], value: A): VisitResult
    def scheduleAfterTx(code: => Unit): Unit

    // reads
    def getFired[A](signal: SignalNode[A]): Option[A]
    def state[A](state: StateNode[A]): A
    def stateAtStart[A](state: StateNode[A]): A

    // structure
    def isChild(parent: Node[_], child: Node[_]): Boolean
    def addChild(parent: Node[_], child: Node[_]): Unit
    def removeChild(parent: Node[_], child: Node[_]): Unit
    def attach(node: Node[_]): Unit
  }

  sealed trait VisitResult
  case object EnqueueChildren extends VisitResult
  case object ReEnqueueSelf   extends VisitResult
  case object Noop            extends VisitResult

  private object ExistingSignalStateUpdates {
    def unapply[A](node: StateNode[A]): Option[SignalStateUpdates[A]] =
      (node.timeline match {
        case tl: EventTimeline => tl.getChildren(node).find(_.isInstanceOf[SignalStateUpdates[_]])
        case _                 => None
      }).asInstanceOf[Option[SignalStateUpdates[A]]]
  }
}
