package io.tocsin.core

import io.tocsin.core.Graph.{SignalNode, StateNode}

private object Attach {
  def apply[A](node: SignalNode[A]): Signal[A] = {
    node.timeline.tx { node.timeline.attach(node) }
    new SignalImpl[A](node)
  }

  def apply[A](node: StateNode[A]): State[A] = {
    node.timeline.tx { node.timeline.attach(node) }
    new StateImpl[A](node)
  }
}
