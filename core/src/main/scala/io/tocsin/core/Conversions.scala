package io.tocsin.core

trait Conversions {
  import scala.language.implicitConversions
  trait <=>[A, B] {
    def to: A => B
    def from: B => A
  }

  def iso[A, B](toF: A => B, fromF: B => A): A <=> B = new (A <=> B) {
    override def to: A => B   = toF
    override def from: B => A = fromF
  }

  implicit def to[A, B](a: A)(implicit iso: A <=> B): B   = iso.to(a)
  implicit def from[A, B](b: B)(implicit iso: A <=> B): A = iso.from(b)
}
