package io.tocsin.scalaz

import io.tocsin.core.Collector
import org.scalatest.{FreeSpec, Matchers, OneInstancePerTest}
import scalaz.Scalaz._
import scalaz.{Kleisli, PlusEmpty}

import scala.Function.const

class SignalSpec extends FreeSpec with OneInstancePerTest with Matchers {
  implicit val timeline: Timeline = Timeline()

  "functor" - {
    "map should morph the value" in {
      val source = Source[Int]
      val mapped = source.signal.map(_ + 5)
      val out    = Collector(mapped)

      source ! (10, 20, 30)

      out.events shouldBe List(15, 25, 35)
    }

    "Law 1: Identity" in {
      val source = Source[Int]
      val mapped = source.signal.map(identity)
      val out    = Collector(mapped)

      source ! (10, 20, 30)

      out.events shouldBe List(10, 20, 30)
    }

    "Law 2: Composition of morphisms" in {
      val source = Source[Int]
      val signal = source.signal

      val f          = (i: Int) => i + 5
      val g          = (i: Int) => i * 2
      val fThenG     = signal.map(f).map(g)
      val fgComposed = signal.map(f andThen g)
      val out1       = Collector(fThenG)
      val out2       = Collector(fgComposed)

      source ! (10, 20, 30)

      out1.events shouldBe out2.events
      out1.events shouldBe List(30, 50, 70)
    }
  }

  "applicative" - {
    "should merge signals when fired in one transaction" in {
      val source = Source[Int]
      val signal = source.signal

      val incremented = signal.map(_ + 1)
      val doubled     = signal.map(_ * 2)
      val zipped      = (incremented |@| doubled)((a, b) => (a, b))

      val out = Collector(zipped)

      source ! (1, 2, 3)

      out.events shouldBe List((2, 2), (3, 4), (4, 6))
    }

    "should have semigroup instance if enclosed value type has semigroup instance" in {
      val source = Source[Int]
      val signal = source.signal

      val incremented = signal.map(_ + 1)
      val doubled     = signal.map(_ * 2)
      val addUp       = incremented |+| doubled

      val out = Collector(addUp)

      source ! (1, 2, 3)

      out.events shouldBe List(2 + 2, 3 + 4, 4 + 6)
    }

    "should fire only when all operands fire" in {
      val source1 = Source[Int]
      val source2 = Source[Int]

      val signal1 = source1.signal
      val signal2 = source2.signal

      val addUp = signal1 |+| signal2
      val out   = Collector(addUp)

      source1 ! 10
      source2 ! 20
      source1 ! 30
      out.events shouldBe List()

      timeline.tx {
        source1 ! 1
        source2 ! 2
      }
      out.events shouldBe List(3)
    }

    "Law 1: Identity" in { //pure id <*> v = v
      val source = Source[Int]
      val signal = source.signal

      val idSignal = signal <*> (identity[Int] _).pure[Signal]
      val out      = Collector(idSignal)

      source ! (1, 2, 3)

      out.events shouldBe List(1, 2, 3)
    }

    "Law 1: Identity expressed as monoid" in {
      val source = Source[Int]
      val signal = source.signal

      val leftId  = 0.pure[Signal] |+| signal
      val rightId = signal |+| 0.pure[Signal]

      val leftCollector  = Collector(leftId)
      val rightCollector = Collector(rightId)

      source ! (1, 2, 3)

      leftCollector.events shouldBe List(1, 2, 3)
      rightCollector.events shouldBe List(1, 2, 3)
    }

    "Law 2: Homomorphism" in { //pure f <*> pure x = pure (f x)
      val f = (i: Int) => i + 5
      val x = 10

      val (left, right) = timeline.tx {
        val leftSide  = x.pure[Signal] <*> f.pure[Signal]
        val rightSide = f(x).pure[Signal]
        (Collector(leftSide, timeline), Collector(rightSide, timeline))
      }

      left.events shouldBe right.events
      left.events shouldBe List(15)
    }

    "Law 3: Interchange" in { //u <*> pure y = pure ($ y) <*> u
      val u = ((i: Int) => i + 5).pure[Signal]
      val y = 10

      val (left, right) = timeline.tx {
        val leftSide  = 10.pure[Signal] <*> u
        val rightSide = u <*> ((f: Int => Int) => f(y)).pure[Signal]
        (Collector(leftSide, timeline), Collector(rightSide, timeline))
      }

      left.events shouldBe right.events
      left.events shouldBe List(15)
    }

    "Law 4: Composition" in { //pure (.) <*> u <*> v <*> w = u <*> (v <*> w)
      val uSource = Source[Int => Int]
      val vSource = Source[Int => Int]
      val wSource = Source[Int]

      val u   = uSource.signal
      val v   = vSource.signal
      val w   = wSource.signal
      val dot = ((f: Int => Int) => (g: Int => Int) => f compose g).pure[Signal]

      val leftSide  = w <*> (v <*> (u <*> dot))
      val rightSide = (w <*> v) <*> u
      val left      = Collector(leftSide)
      val right     = Collector(rightSide)

      timeline.tx {
        uSource ! ((i: Int) => i * 2)
        vSource ! ((i: Int) => i + 5)
        wSource ! 10
      }

      left.events shouldBe right.events
      left.events shouldBe List((10 + 5) * 2)
    }
  }

  "universally quantified monoid" - {
    "Law 1: Associativity" in { // (x <> y) <> z = x <> (y <> z)
      val xSource = Source[Int]
      val ySource = Source[Int]
      val zSource = Source[Int]

      val x = xSource.signal
      val y = ySource.signal
      val z = zSource.signal

      val leftSide  = (x <+> y) <+> z
      val rightSide = x <+> (y <+> z)
      val left      = Collector(leftSide)
      val right     = Collector(rightSide)

      timeline.tx { xSource ! 1; ySource ! 100; zSource ! 100 }
      timeline.tx { ySource ! 2; zSource ! 100 }
      timeline.tx { zSource ! 3 }

      left.events shouldBe right.events
      left.events shouldBe List(1, 2, 3)
    }

    "Law 2: identity" in {
      // mempty <> x = x
      // x <> mempty = x

      val source = Source[Int]
      val signal = source.signal

      val rightId = signal <+> mempty[Signal, Int]
      val leftId  = mempty[Signal, Int] <+> signal

      val original = Collector(signal)
      val right    = Collector(rightId)
      val left     = Collector(leftId)

      source ! (1, 2, 3)

      right.events shouldBe original.events
      left.events shouldBe original.events
      original.events shouldBe List(1, 2, 3)
    }

    "should chose one (first) if fired in one transaction, and all if fired from separate transactions" in {
      val source1 = Source[Int]
      val source2 = Source[Int]

      val choose = source1.signal <+> source2.signal
      val out    = Collector(choose)

      timeline.tx {
        source1 ! 0
        source2 ! 100
      }

      source1 ! 1
      source2 ! 2

      out.events shouldBe List(0, 1, 2)
    }

    "fold traversable of signals" in {
      val source1 = Source[Int]
      val source2 = Source[Int]
      val source3 = Source[Int]
      val source4 = Source[Int]
      val source5 = Source[Int]

      val folded = List(source1.signal, source2.signal, source3.signal, source4.signal, source5.signal)
        .suml(implicitly[PlusEmpty[Signal]].monoid)
      val out = Collector(folded)

      timeline.tx { source1 ! 1; source5 ! 100 }
      timeline.tx { source2 ! 2; source5 ! 100 }
      timeline.tx { source3 ! 3; source5 ! 100 }
      timeline.tx { source4 ! 4; source5 ! 100 }
      source5 ! 5

      out.events shouldBe List(1, 2, 3, 4, 5)
    }
  }

  "monad" - {
    "Law 1: Left identity" in { // return a >>= f = f a
      val source = Source[Int]
      val f      = (i: Int) => source.signal <+> Signal.always(i)
      val a      = 10

      val (left, right) = timeline.tx {
        val leftSide  = a.pure[Signal] >>= f
        val rightSide = f(a)
        val left      = Collector(leftSide, timeline)
        val right     = Collector(rightSide, timeline)
        (left, right)
      }

      source ! (1, 2, 3)

      left.events shouldBe right.events
      left.events shouldBe List(10, 1, 2, 3)
    }
    "Law 2: Right identity" in { // m >>= return = m
      val source = Source[Int]

      val signal = source.signal >>= (a => a.pure[Signal])
      val out    = Collector(signal)

      source ! (1, 2, 3)
      out.events shouldBe List(1, 2, 3)
    }
    "Law 3: Associativity" in {
      // (m >>= f) >>= g = m >>= (\x -> f x >>= g)
      // or:
      // (f >=> g) >=> h = f >=> (g >=> h)

      import scala.language.{higherKinds, implicitConversions}
      implicit def toKleisli[M[_], A, B](f: A => M[B]): Kleisli[M, A, B] = Kleisli(f)

      val source = Source[Int]

      val f = (i: Int) => Signal.once(i) <+> source.signal
      val g = (i: Int) => (i + 5).pure[Signal]
      val h = (i: Int) => Signal.always(i)

      val leftComposed  = (f >=> g) >=> h
      val rightComposed = f >=> (g >=> h)

      val (left, right) = timeline.tx {
        val leftSide  = leftComposed(10)
        val rightSide = rightComposed(10)
        source ! 1
        (Collector(leftSide), Collector(rightSide))
      }

      source ! (2, 3)

      left.events shouldBe right.events
      left.events shouldBe List(15, 7, 8)
    }

    "several flatten use cases" in {
      val source = Source[Int]
      val signal = source.signal

      val flattenedOnce   = signal.map(Signal.once).join
      val flattenedAlways = signal.map(Signal.always).join
      val filtered        = signal.filter(_ != 2)
      val empty           = signal >>= const(Signal.never[Int])

      val collector1 = Collector(flattenedOnce)
      val collector2 = Collector(flattenedAlways)
      val collector3 = Collector(filtered)
      val collector4 = Collector(empty)

      source ! (1, 2, 3)

      collector1.events shouldBe List(1, 2, 3)
      collector2.events shouldBe List(1, 2, 3)
      collector3.events shouldBe List(1, 3)
      collector4.events shouldBe List()
    }

    "flatten signal with options" in {
      val source = Source[Option[Int]]
      val signal = source.signal.unite
      val out    = Collector(signal)

      source ! (Some(1), None, Some(3))

      out.events shouldBe List(1, 3)
    }

    "flatten lists taking optional heads" in {
      val source = Source[List[Int]]
      val signal = source.signal.unite
      val out    = Collector(signal)

      source ! List(1, 2, 3)
      source ! List()
      source ! List(4)
      source ! List(5, 6, 7)

      out.events shouldBe List(1, 4, 5)
    }

    "monad chooses signal fired in the same transaction" in {
      val switch  = Source[Int]
      val source1 = Source[Int]
      val source2 = Source[Int]
      val source3 = Source[Int]

      val signal1 = source1.signal
      val signal2 = source2.signal
      val signal3 = source3.signal

      val switchSignal = switch.signal >>= {
        case 1 => signal1
        case 2 => signal2
        case 3 => signal3
        case _ => Signal.never[Int]
      }

      val collector = new Collector(switchSignal.sink)

      // black hole
      timeline.tx {
        source1 ! 1
        source2 ! 2
        source3 ! 3
      }

      // serve only from source 1
      timeline.tx {
        switch ! 1
        source1 ! 4 // fire
        source2 ! 5
        source3 ! 6
      }

      // serve only from source 2
      timeline.tx {
        switch ! 2
        source1 ! 7
        source2 ! 8 // fire
        source3 ! 9
      }

      // serve only from source 3
      timeline.tx {
        switch ! 3
        source1 ! 10
        source2 ! 11
        source3 ! 12 // fire
      }

      // black hole again
      timeline.tx {
        switch ! 0
        source1 ! 13
        source2 ! 14
        source3 ! 15
      }

      collector.events shouldBe List(4, 8, 12)
    }

    "monad doen't choose signal if fires don't happen in one transaction" in {
      val switch  = Source[Int]
      val source1 = Source[Int]
      val source2 = Source[Int]
      val source3 = Source[Int]

      val signal1 = source1.signal
      val signal2 = source2.signal
      val signal3 = source3.signal

      val switchSignal = switch.signal >>= {
        case 1 => signal1
        case 2 => signal2
        case 3 => signal3
        case _ => Signal.never[Int]
      }

      val out = Collector(switchSignal)

      source1 ! 1
      source2 ! 2
      source3 ! 3
      switch ! 1
      source1 ! 4
      source2 ! 5
      source3 ! 6
      switch ! 2
      source1 ! 7
      source2 ! 8
      source3 ! 9
      switch ! 3
      source1 ! 10
      source2 ! 11
      source3 ! 12
      switch ! 0
      source1 ! 13
      source2 ! 14
      source3 ! 15

      // nothing happens, because switching and firing of proxy should happen in the same tx
      out.events shouldBe List()
    }
  }

  "zip" - {
    "two signals zip with each other only if fired in the same transaction" in {
      val source1 = Source[Int]
      val source2 = Source[String]

      val zipSelf  = Collector(source1.signal fzip source1.signal)
      val zip1and2 = Collector(source1.signal fzip source2.signal)

      source1 ! 1
      source2 ! "one"
      source1 ! 2
      source2 ! "two"

      timeline.tx {
        source1 ! 3
        source2 ! "three"
      }

      zipSelf.events shouldBe List((1, 1), (2, 2), (3, 3))
      zip1and2.events shouldBe List((3, "three"))
    }
  }
}
