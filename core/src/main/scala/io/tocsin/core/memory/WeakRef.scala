package io.tocsin.core.memory

import java.lang.ref.WeakReference

class WeakRef[T](value: T) {
  val underlying: WeakReference[T] = new WeakReference[T](value)

  def get = Option(underlying.get)
  def apply(): T = {
    val ret = underlying.get
    if (ret == null) throw new NoSuchElementException
    ret
  }
  def clear(): Unit          = underlying.clear()
  def enqueue: Boolean       = underlying.enqueue
  def isEnqueued: Boolean    = underlying.isEnqueued
  def self: WeakReference[T] = underlying
}

object WeakRef {
  def apply[T](t: T)                         = new WeakRef(t)
  def unapply[T](arg: WeakRef[T]): Option[T] = arg.get
}
