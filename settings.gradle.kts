rootProject.name = "tocsin"

include(
    *Projects.values().map { it.path() }.toTypedArray()
)
