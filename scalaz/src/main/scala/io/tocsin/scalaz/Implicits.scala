package io.tocsin.scalaz

import io.tocsin.core.Primitives

object Implicits {
  implicit lazy val global: Timeline = Primitives.globalTimeline
}
