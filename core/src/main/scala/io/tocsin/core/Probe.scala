package io.tocsin.core

private[tocsin] sealed trait Probe[A]

private class ProbeImpl[A](state: StateImpl[A], timeline: EventTimeline) extends Probe[A] {
  timeline.tx { timeline.attach(state.node) }
  def sample: A = timeline.newTx { timeline.state(state.node) }
}

private object Probe {
  def apply[A](state: StateImpl[A]): Probe[A] = new ProbeImpl[A](state, state.node.timeline.orElseGlobal)
  def apply[A](state: StateImpl[A], timeline: Timeline): Probe[A] = {
    if (!timeline.isInstanceOf[EventTimeline]) throw EventTimelineRequired
    new ProbeImpl[A](state, timeline.asInstanceOf[EventTimeline])
  }
}
