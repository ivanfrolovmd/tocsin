plugins { `kotlin-dsl` }

setupScala()

dependencies {
  compile(Projects.core)
  compile(Libs.scalaz)

  testCompile(Projects.debug)
}
