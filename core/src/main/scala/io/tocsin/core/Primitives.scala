package io.tocsin.core

import io.tocsin.core.Graph._

import scala.concurrent.ExecutionContext

private[tocsin] object Primitives {
  lazy val globalTimeline: Timeline = new EventTimeline

  def createTimeline: Timeline                         = new EventTimeline
  def tx[Z](timeline: Timeline, code: => Z): Z         = timeline.tx(code)
  def createSource[A](timeline: Timeline): Source[A]   = Source(timeline)
  def send[A, AA <: A](source: Source[A], a: AA): Unit = source.send(a)
  def signal[A](source: Source[A]): Signal[A]          = source.signal

  def sink[A](signal: Signal[A]): Sink[A]                       = Sink(signal)
  def sink[A](signal: Signal[A], timeline: Timeline): Sink[A]   = Sink(signal, timeline)
  def listen[A](sink: Sink[A], cb: A => Unit): Listener         = sink.listen(cb, weak = false, once = false)
  def listenOnce[A](sink: Sink[A], cb: A => Unit): Listener     = sink.listen(cb, weak = false, once = true)
  def listenWeak[A](sink: Sink[A], cb: A => Unit): Listener     = sink.listen(cb, weak = true, once = false)
  def listenOnceWeak[A](sink: Sink[A], cb: A => Unit): Listener = sink.listen(cb, weak = true, once = true)
  def listenAsync[A](sink: Sink[A], cb: A => Unit, ec: ExecutionContext): Listener =
    sink.listen(a => ec.execute(() => cb(a)), weak = false, once = false)
  def unregister(listener: Listener): Unit = listener.unregister()

  def probe[A](state: State[A]): Probe[A]                     = Probe(state)
  def probe[A](state: State[A], timeline: Timeline): Probe[A] = Probe(state, timeline)
  def sample[A](probe: Probe[A]): A                           = probe.sample

  def signalNever[A]: Signal[A]            = Attach(SignalNever())
  def signalOnce[A](value: A): Signal[A]   = Attach(SignalOnce(value))
  def signalAlways[A](value: A): Signal[A] = Attach(SignalAlways(value))

  def signalMap[A, B](signal: Signal[A], f: A => B): Signal[B] =
    Attach(SignalMap(signal.node, f))
  def signalApply[A, B, C](fa: Signal[A], fb: Signal[B], f: (A, B) => C): Signal[C] =
    Attach(SignalApply(fa.node, fb.node, f))
  def signalPlus[A](fa: Signal[A], fb: Signal[A]): Signal[A] =
    Attach(SignalPlus(fa.node, fb.node))
  def signalFilter[A](signal: Signal[A], predicate: A => Boolean): Signal[A] =
    Attach(SignalFilter(signal.node, predicate))
  def signalBind[A, B](signal: Signal[A], f: A => Signal[B]): Signal[B] =
    Attach(SignalMonad(SignalMap(signal.node, (a: A) => f(a).node)))

  def stateSignalSwitch[A](state: State[Signal[A]]): Signal[A] =
    Attach(SignalSwitch(StateMap(state.node, (s: Signal[A]) => s.node)))
  def signalWithGate[A](signal: Signal[A], gate: => State[Boolean]): Signal[A] =
    Attach(SignalWithGate(signal.node, gate.node))
  def signalSnapshot[A, B, C](signal: Signal[A], state: => State[B], f: (A, B) => C): Signal[C] =
    Attach(SignalSnapshot(signal.node, state.node, f))
  def stateUpdates[A](state: State[A]): Signal[A] =
    Attach(SignalStateUpdates(state.node))

  def stateConst[A](value: A): State[A] =
    Attach(StateConst(value))
  def stateHold[A](signal: Signal[A], zero: A): State[A] =
    Attach(StateHold(signal.node, zero))
  def stateCalmed[A](state: State[A], equality: (A, A) => Boolean): State[A] =
    Attach(StateCalmed(state.node, equality))

  def stateMap[A, B](state: State[A], f: A => B): State[B] =
    Attach(StateMap(state.node, f))
  def stateApply[A, B, C](fa: State[A], fb: State[B], f: (A, B) => C): State[C] =
    Attach(StateApply(fa.node, fb.node, f))
  def stateBind[A, B](state: State[A], f: A => State[B]): State[B] =
    Attach(StateMonad(StateMap(state.node, (a: A) => f(a).node)))

  private implicit def sourceIso[A]: Source[A] <=> SourceImpl[A] = iso(_.asInstanceOf[SourceImpl[A]], identity)
  private implicit def signalIso[A]: Signal[A] <=> SignalImpl[A] = iso(_.asInstanceOf[SignalImpl[A]], identity)
  private implicit def stateIso[A]: State[A] <=> StateImpl[A]    = iso(_.asInstanceOf[StateImpl[A]], identity)
  private implicit def sinkIso[A]: Sink[A] <=> SinkImpl[A]       = iso(_.asInstanceOf[SinkImpl[A]], identity)
  private implicit def probeIso[A]: Probe[A] <=> ProbeImpl[A]    = iso(_.asInstanceOf[ProbeImpl[A]], identity)
  private implicit val listenerIso: Listener <=> ListenerImpl    = iso(_.asInstanceOf[ListenerImpl], identity)

}
