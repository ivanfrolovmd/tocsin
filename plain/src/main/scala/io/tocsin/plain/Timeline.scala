package io.tocsin.plain

import io.tocsin.core
import io.tocsin.core.Primitives

class Timeline(val timeline: core.Timeline) {
  def tx[Z](code: => Z): Z = Primitives.tx(timeline, code)
}
object Timeline {
  def apply(): Timeline              = Primitives.createTimeline
  implicit lazy val global: Timeline = Primitives.globalTimeline
}
